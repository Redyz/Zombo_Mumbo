#include "utility.hpp"
#include <iostream>
typedef std::chrono::high_resolution_clock precise_clock;
Timer::Timer(std::string message) : ms_length(0), stopped(false) {
  this->message = message;
  start_time = precise_clock::now();
}

Timer::~Timer()
{
  if(!stopped){
    stop();
    if(ms_length > 1)
      std::cout << get_final_message() << std::endl;
  }
}

std::string Timer::stop(){
  stopped = true;
  auto end_time = precise_clock::now();
  auto diff = end_time - start_time;
  auto human_time = std::chrono::duration_cast<std::chrono::milliseconds>(diff);
  char buffer[200];
  ms_length = (int)human_time.count();
  sprintf(buffer, "'%s' took %i ms to complete", message.c_str(), ms_length);
  final_message = std::string(buffer);
  return final_message;
}
