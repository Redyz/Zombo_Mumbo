#pragma once
#include <string>
#include <SFML/System.hpp>
#include "enums.hpp"
struct Entity;

enum ITEM_TYPE
{
  ITEM_TYPE_NONE = 0,
  ITEM_TYPE_MEDKIT = 1 << 1,
  ITEM_TYPE_GUN = 1 << 2,
  ITEM_TYPE_SHOTGUN = 1 << 3,
  ITEM_TYPE_UZI = 1 << 4,
  ITEM_TYPE_UZI_AKIMBO = 1 << 5,
  ITEM_TYPE_PIZZA = 1 << 6,
  ITEM_TYPE_GROUP_MULTIPLE = ITEM_TYPE_MEDKIT,
};

class Item
{
public:
  virtual std::string get_label(){ return "no_label"; };
  virtual void update(){};
  virtual bool use(Entity& user, float dt){ return true; };
  virtual int get_type() { return ITEM_TYPE_NONE; };
  std::string visual_label;
  unsigned int last_use = 0;
  sf::Vector2f position, offset = {0, 0};
  bool consume = true;
  bool two_handed = false;
  int group = 0;
  float scale = 1;
};

class Medkit : public Item
{
public:
  Medkit()
  {
    group = ENTITY_GROUP_MEDKIT;
    visual_label = "medkit";
  }
  virtual std::string get_label() override { return "Medkit"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_MEDKIT; };
private:
  int healing_amount = 500;
};

class Pizza : public Item
{
public:
  Pizza()
  {
    visual_label = "pizza";
  }
  virtual std::string get_label() override { return "Pizza"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_PIZZA; };
};

class Gun : public Item
{
public:
  Gun()
  {
    consume = false;
  }
  virtual void update() override;
  virtual std::string get_label() override { return "Gun"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_GUN; };
  virtual int get_clip_size() { return 0; };
  int ammo = 99999;
  unsigned int rate = 1;
};

class Glock : public Gun
{
public:
  Glock()
  {
    ammo = -1;
    rate = 25;
    visual_label = "glock";
  }
  
  virtual std::string get_label() override { return "Glock"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_GUN; };
};

class Uzi : public Gun
{
public:
  Uzi()
  {
    ammo = get_clip_size();
    rate = 7;
    visual_label = "uzi";
    offset = sf::Vector2f(10, 0);
  }
  
  virtual std::string get_label() override { return "Uzi"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_UZI; };
  virtual int get_clip_size() override { return 20; };
};

class UziAkimbo : public Gun
{
public:
  UziAkimbo()
  {
    ammo = get_clip_size();
    rate = 3;
    visual_label = "uzi_akimbo";
    offset = sf::Vector2f(0, 0);
    two_handed = true;
    
  }
  
  virtual std::string get_label() override { return "Uzi Akimbo"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_UZI_AKIMBO; };
  virtual int get_clip_size() override { return 40; };
};


class Shotgun : public Gun
{
public:
  Shotgun()
  : spread(7)
  {
    ammo = get_clip_size();
    rate = 70;
    visual_label = "shotgun";
    two_handed = true;
    scale = 0.7f;
  }
  virtual std::string get_label() override { return "Shotgun"; }; 
  virtual bool use(Entity& user, float dt) override;
  virtual int get_type() override { return ITEM_TYPE_SHOTGUN; };
  virtual int get_clip_size() override { return 8; };
  
  int spread;
};
