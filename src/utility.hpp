#pragma once
#include "options.hpp"

#ifdef UNIX
#include <dirent.h>
#else
#include <Windows.h>
#endif

#include <string.h>
#include "structs.hpp"
#include <Thor/Vectors.hpp>
#include <chrono>
typedef sf::Vector2f v2f;
typedef sf::Vector3f v3f;

static v3f v3f_c(v2f base)
{
  return v3f(base.x, base.y, 0);
}

static v3f v3f_c(v2f base, float last)
{
  return v3f(base.x, base.y, last);
}


struct v2f_t {
  float x = 0.0f, y = 0.0f;
  v2f_t() { }
  v2f_t(float _x, float _y) : x(_x), y(_y) { }
  v2f_t(float _xy) : x(_xy), y(_xy) { }
  v2f_t(const sf::Vector2f& v) : x(v.x), y(v.y) { }
  v2f_t(const sf::Vector2i& v) : x((float)v.x), y((float)v.y) { }
  v2f_t operator + (const v2f_t& other) const { return v2f_t(x + other.x, y + other.y); }
  v2f_t operator - (const v2f_t& other) const { return v2f_t(x - other.x, y - other.y); }
  v2f_t operator * (const v2f_t& other) const { return v2f_t(x * other.x, y * other.y); }
  v2f_t operator / (const v2f_t& other) const { return v2f_t(x / other.x, y / other.y); }
  operator sf::Vector2f() const { return sf::Vector2f(x, y); }
};
#define D1(var) std::cerr << #var << " " << var << "\n";
#define D2(var) std::cerr << #var << " " << var.x << " " << var.y << "\n";

namespace Math
{
  static float get_angle_between_coordinates(float deltax, float deltay)
  {
    float angle;
    angle = std::atan(deltay/deltax) * 180.0f / 3.1415f;
    if(deltax < 0) angle += 180;
    if(deltay < 0) angle += 360;
    angle += 90;
    return fmod(angle, 360.f);
  }
  
  static float get_angle_between_coordinates(float afx, float afy, float bfx, float bfy)
  {
    return get_angle_between_coordinates(afx-bfx, afy-bfy);
  }
  
  static float get_angle_between_coordinates(v2f a, v2f b)
  {
    return get_angle_between_coordinates(a.x, a.y, b.x, b.y);
  }
  
  static sf::Vector2f norm(sf::Vector2f in)
  {
    float mag = std::sqrt(in.x*in.x + in.y*in.y);
    if(mag < 0.1)
      return sf::Vector2f(0, 0);
    return sf::Vector2f(in.x/mag, in.y/mag);
  }
}

static bool circle_collision(Entity& e, Entity& ee, float &outIntersect)
{
  v2f d = e.position - ee.position;
  float dist = std::sqrt(d.x*d.x + d.y*d.y);
  outIntersect = dist-std::abs(e.w + ee.w);
  return (dist < e.w + ee.w);
}


static bool circle_collision(Entity& e, Entity& ee)
{
  float nocare;
  return circle_collision(e, ee, nocare);
}


static bool line_collision(Line& one, Line& two)
{
  float denominator = ((one.to.x - one.from.x) * (two.to.y - two.from.y)) - ((one.to.y - one.from.y) * (two.to.x - two.from.x));
  float numerator1 = ((one.from.y - two.from.y) * (two.to.x - two.from.x)) - ((one.from.x - two.from.x) * (two.to.y - two.from.y));
  float numerator2 = ((one.from.y - two.from.y) * (one.to.x - one.from.x)) - ((one.from.x - two.from.x) * (one.to.y - one.from.y));
  
  // Detect coincident lines (has a problem, read below)
  if (denominator == 0) return numerator1 == 0 && numerator2 == 0;
  
  float r = numerator1 / denominator;
  float s = numerator2 / denominator;
  
  return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
}

static inline sf::Vector2f closest_point_on_line(v2f p, Line line)
{
  v2f n = line.to - line.from;
  v2f pa = line.from - p;
  float c = thor::dotProduct(n, pa);
  if(c > 0.0f)
  { 
    return line.from;
  }
  v2f bp = p - line.to;
  if(thor::dotProduct(n, bp) > 0.0f)
  { 
    return line.to;
  }
  v2f e = pa - n*(c/thor::squaredLength(n));
  return p+e;
}

#include <string>
#include <sstream>
#include <vector>
#include <iterator>

template<typename Out>
static void split(const std::string &s, char delim, Out result) {
  std::stringstream ss;
  ss.str(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    *(result++) = item;
  }
}

// https://stackoverflow.com/questions/236129/most-elegant-way-to-split-a-string
static std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, std::back_inserter(elems));
  return elems;
}

static ZPolygon create_from_points(std::vector<sf::Vector2f> points)
{
  ZPolygon polygon;
  auto pastItr = points.begin();
  sf::Vector2f past = *pastItr, first = *pastItr;
  points.erase(pastItr);
  while(!points.empty())
  {
    auto currentItr = points.begin();
    sf::Vector2f current = *currentItr;
    points.erase(currentItr);
    polygon.lines.push_back(Line(past.x, past.y, current.x, current.y));
    past = current;
  }
  
  polygon.lines.push_back(Line(past.x, past.y, first.x, first.y));
  return polygon;
}

static inline sf::Vector2f v(float x, float y)
{
  return sf::Vector2f(x, y);
}

static inline v2f normal(const v2f vec)
{
  return v2f(-vec.y, vec.x);
}

static inline float magnitude(const v2f vec)
{
  return std::sqrt(vec.x*vec.x + vec.y*vec.y);
}

static inline sf::Vector2f lerp(const sf::Vector2f& a, float t, const sf::Vector2f& b)
{
  return t*(b-a)+a;
}

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define LERROR(x) std::cout << "[ERR:" TOSTRING(__LINE__) "]: " << x << std::endl;
#define CSTR(x) dynamic_cast< std::ostringstream & >(( std::ostringstream() << std::dec << x ) ).str().c_str()
#define SSTR(x) dynamic_cast< std::ostringstream & >(( std::ostringstream() << std::dec << x ) ).str()
#define DEB(tim, code) do{ static float goal = 0; if(gGame.time.getElapsedTime().asSeconds() > goal){ goal = gGame.time.getElapsedTime().asSeconds()+((float)(FPS_TICK*tim)); code; } }while(0);
#define VDEB(time, code) do{ static int last_used = 0; if(visual_tick - last_used > time){ code; last_used = visual_tick; } }while(0);

#include <sys/types.h>
#include <sys/stat.h>

#include <cstdlib>

	std::string save_file_dialog(std::string title, std::string pattern, std::string description, std::string default_path = "");
	std::string open_file_dialog(std::string title, std::string pattern, std::string description, std::string default_path = "");

	std::string get_settings_filepath();

	static std::vector<std::string> get_filenames_in_directory(std::string directory)
	{
		std::vector<std::string> to_return;
#ifdef UNIX

		DIR* pDirectory;
		struct dirent *ent;
		if ((pDirectory = opendir(directory.c_str())) != nullptr)
		{
			while ((ent = readdir(pDirectory)) != nullptr)
			{
				if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0)
					to_return.push_back(ent->d_name);
			}
			closedir(pDirectory);
		}
#else
		HANDLE hFind;
		WIN32_FIND_DATA data;

		hFind = FindFirstFile(CSTR(directory.c_str() << "\\*.*"), &data);
		if (hFind != INVALID_HANDLE_VALUE) {
			do {
				std::string lazy = data.cFileName;
				if(lazy != "."  && lazy != "..")
					to_return.push_back(data.cFileName);
			} while (FindNextFile(hFind, &data));
			FindClose(hFind);
		}
#endif

		return to_return;
		}
#ifdef UNIX
	inline std::string get_home_directory()
	{
		std::string home_path = getenv("HOME");
		return home_path;
	}

	inline void create_directory(std::string directory)
	{
		mkdir(directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

#else
#include <Windows.h>
	inline std::string get_home_directory()
	{
		std::string home_path = getenv("USERPROFILE");
		return home_path;
	}

	inline void create_directory(std::string directory)
	{
		CreateDirectory(directory.c_str(), NULL);
	}

#endif

	inline std::string get_settings_filepath()
	{
		std::string directory = CSTR(get_home_directory() << "/.config/"), full_path = SSTR(directory << "zumbo.conf");
		create_directory(directory);
		return full_path;
	}
	


// Converts degrees to radians.
#define rad(angleDegrees) (angleDegrees * PI / 180.0)

// Converts radians to degrees.
#define deg(angleRadians) (angleRadians * 180.0 / PI)

#include <chrono>

class Timer{
public:
  Timer(std::string message);
  ~Timer();
  std::string stop();
  std::string get_final_message() { return final_message; }
  int ms_length;
private:
  bool stopped;
  std::string message;
  std::string final_message;
  std::chrono::high_resolution_clock::time_point start_time;
};

#define STIMER_BEGIN(name) { Timer auto_timer_scoped("SAT - " name);
#define STIMER_END auto_timer_scoped.stop(); if(auto_timer_scoped.ms_length > 1) std::cout << auto_timer_scoped.get_final_message() << std::endl; }

#define TIMER_BEGIN(name) Timer name("AT - " #name);
#define TIMER_END(name) name.stop(); if(name.ms_length > 0) std::cout << name.get_final_message() << std::endl;
