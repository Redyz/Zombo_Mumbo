#pragma once

#include <memory>

typedef unsigned int uint;
template<typename T>
using sp = std::shared_ptr<T>;
