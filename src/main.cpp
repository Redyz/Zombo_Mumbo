#include "options.hpp"
#include "game.hpp"
#include <chrono>
#include <thread>
#include <iostream>
#include <SFML/System.hpp>
#include "utility.hpp"

#ifdef WIN32
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
// no console
#endif

int main(int argc, char** argv)
{
  std::cout << argv[0] << std::endl;
  sf::Clock clock, total;

  if(argc > 1)
  {
    gGame.reset_game(argv[1]);
  }else
    gGame.reset_game(SSTR("assets/maps/" << gGame.level_names[gGame.selected_level]));

  float current_time = total.getElapsedTime().asSeconds();
  while(gGame.status != QUITTING)
  {
    float new_time = total.getElapsedTime().asSeconds();
    float frame_time = new_time - current_time;
    current_time = new_time;

    gGame.clear();

    gGame.input(frame_time);

    DEB(1, gGame.visual_tick++;);

    if(gGame.status == RUNNING)
      gGame.step(frame_time);

    gGame.draw(frame_time);
  }

  gGame.destroy();

  #ifndef UNIX
  // Somehow the process doesn't close under windows and I can't be bothered to find out why
  auto pid = GetCurrentProcessId();
  HANDLE handy;
  handy = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, TRUE, pid);
  TerminateProcess(handy, 0);
  #endif

  return 0;
}
