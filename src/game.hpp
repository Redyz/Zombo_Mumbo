#pragma once
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics.hpp>
#include <functional>
#include <iostream>
#include <memory>
#include <cmath>
#include <list>

#include "typedefs.hpp"
#include "enums.hpp"
#include "structs.hpp"

/*
- windows build
- tinyfiledialog errors?
- resolution in settings
- show editor in settings
- fix player bug
- right mouse click grenade hold
*/

// update FRAG as well
#define MAX_LIGHT 20
#define TEMP_LIFETIME 1.0f
#define MOUSE_BUTTONS 5
#define EPSILON 0.01f
#define NUKLEAR_ENABLED 1
#define FPS_TICK 1.0f/60.0f
#define MAX_POLYGON 1000

// EDITOR CONSTS
#define NODE_RADIUS 5
#define MAX_EDITOR_MOVE_FACTOR 45
#define BOTTOM_PANEL_HEIGHT 55

struct nk_context;
struct nk_font_atlas;
struct editor_action;
class Item;

class Game
{
public:
  static constexpr const char* TITLE = "ZuMbO MuMBo";
  
  Game();
  void destroy();
  void draw(float dt);
  void draw_osd(float dt);
  void input(float dt);
  void step(float dt);
  void clear();
  
  int get_id() { return id++;  }

  void add_action(std::shared_ptr<editor_action> to_add);
  void set_zoom(float absolute);
  void zoom(float modifier, bool force = false);
  float get_zoom();
  
  void reset_game(std::string filename);
  
  void parse_map(std::string filename);
  void save_map(std::string filename);
  void clear_map();
  void load_settings();
  void save_settings();
  
  void reset_window();
  
  bool is_in_editor() { return status == PAUSED && settings.show_editor; }
  bool is_editor_click();
  
  void create_particle(sf::Vector2f start, sf::Vector2f target, float speed = 800.0f, int type = 0);
  void create_projectile(Entity& parent, sf::Vector2f target, int projectile_type = PROJECTILE_BULLET);
  void create_grenade(Entity& parent, sf::Vector2f target, float speed = 30.0f);
  void create_decal(sf::Vector2f where, float rotation, std::string label);
  void create_mob(sf::Vector2f where);
  void create_spawner(sf::Vector2f where);
  void create_navnode(sf::Vector2f where);
  
  void next_wave();
  
  void map_message(std::string mess, sf::Vector2f position, sf::Color color = sf::Color::White);
  void center_message(std::string mess, sf::Color color = sf::Color::White, int font_size = 60);
  void bottom_message(std::string mess, sf::Color color = sf::Color::White, int font_size = 30, sf::Color flashColor = sf::Color::Yellow, int lifetime = 40);
  
  bool raycast(sf::Vector2f start, ZPolygon& touch, sf::Vector2f target, int collide_groups = POLYGON_GROUP_COMBINE_COLLIDE, ZPolygon* ignore = nullptr);
  bool raycast(sf::Vector2f start, float& length, sf::Vector2f target, int collide_groups = POLYGON_GROUP_COMBINE_COLLIDE, ZPolygon* ignore = nullptr);
  bool raycast(sf::Vector2f start, sf::Vector2f& touch, sf::Vector2f target, int collide_groups = POLYGON_GROUP_COMBINE_COLLIDE, ZPolygon* ignore = nullptr);
  
  void play_sound(std::string name, float volume);
  void play_sound(std::string name, sf::Vector2f position = sf::Vector2f(0, 0), float volume = 100.0f);

  bool is_round_finished();
  bool is_in_map_selection();
  
  void set_logic(GAME_LOGIC_STATUS new_logic);
  Entity& player();
  int tick = 0, visual_tick = 0;
  GAME_STATUS status = RUNNING;
  GAME_LOGIC_STATUS logic_status = LOGIC_MENU, previous_logic_status = LOGIC_ONGOING;
  MENU_STATUS menu_status = MENU_MAIN;
  
  int score = 0, zombies_killed = 0, bullets_fired = 0, zombies_left = 0;  
  bool initial_zooming_completed = false;
  float zoom_current = 0.49f;
  int current_round = 0, total_rounds = 7;
  
  bool changed_map = false;
  float previous_zoom = -1.0f;
  
  // map stuff
  std::vector<Entity> entities, decals;
  std::vector<Spawner> spawners;
  std::vector<sp<Item>> items;
  std::vector<Node> navnodes;
  std::vector<sf::Vector3f> static_lights;
  std::map<std::string, int> configs;
  sf::Vector2f get_mouse_pos();
  
  std::vector<std::string> level_names;
  int selected_level = 0; 
  
  std::vector<sp<editor_action>> editor_actions;
  
  settings_struct settings;
  sfml_struct sfml;
  rules_struct rules;
  effects_struct effects;
  stats_struct stats;
  
  sf::Music ambient_track, ending_track;
  
  ZPolygon ghost_poly, interact_poly;
  ZPolygon* hovered;
  ZPolygon* selected;
  
  sf::Clock time;
  
  std::map<std::string, ZPolygon> predefined_polys;
  
  sf::Vector2f camera_pos, pause_position;
  std::vector<ZPolygon> obstacles;
  std::vector<sf::Vector2f> ed_points;
private:
  void draw_vector(sf::Vector2f from, sf::Vector2f to);
  void draw_vector(Line line);
  void draw_circle(float x, float y, float r, sf::Color color = sf::Color::Red, bool fill = true);
  void draw_line(float ax, float ay, float bx, float by, sf::Color tint = sf::Color::Yellow, float size = 1.0f);
  void draw_sprite(float x1, float y1, float x2, float y2, std::string decal, float rotation = 0, sf::Vector2f scale = {1.0f, 1.0f}, sf::Color tint = sf::Color(255, 255, 255, 255), bool apply_shader = false);
  void draw_polygon(ZPolygon& poly, sf::Color over = sf::Color(0, 0, 0), float thick = 0.5f);
  void entity_step(Entity& e, float dt);
  void draw_blood(Entity& e);
  
  void draw_editor();
  
  int ed_angle_lock = 1;
  int ed_poly_group = POLYGON_GROUP_COLLIDE;
  int ed_float_grid = 0;
  int ed_mode = 0;
  int ed_submode = 0;
  bool ed_dirty_state = true;
  float ed_move_factor = 1.1f;
  float ed_rotation_factor = 5.0f;
  int menu_selection = 0;
  int show_shop = false;
  float current_throw_amount = 0.0f;
  sf::Vector2f ed_clamped_mouse = sf::Vector2f(0, 0);
  std::string ed_poly_texture = "", ed_poly_texture_hovered = "";
  
  std::vector<shop_item> shop_items;
  
  int id = 0;

  screen_message message, bottom_message_t;

  std::vector<std::shared_ptr<sf::Sound>> play_queue;
  std::map<std::string, sf::SoundBuffer> sounds;
  std::vector<Particle> particles;
  std::map<std::string, sf::Texture> textures;
  std::map<std::string, sf::Sprite> sprites;
  int mouse_down[MOUSE_BUTTONS], mouse_down_last[MOUSE_BUTTONS], mouse_last_press[MOUSE_BUTTONS], key_modifiers[50], keys_down[255], keys_down_last[255];
  sf::RenderWindow sfWindow;
  sf::View view, sfui;
  
  bool initial_menu_shown = false;
  bool menu_left_requested = false, menu_right_requested = false;
  int menu_size;
  std::vector<menu_item> menu_items;
#if NUKLEAR_ENABLED
  nk_context *ctx;
  nk_font_atlas *atlas;
#endif
  
  std::vector<Entity> to_add;
  
  std::set<std::string> zombie_names;
  sf::RenderTexture renderbuffer, uiBuffer;
  sf::Shader shadow_shader;
  std::map<int, std::string> mPolygonMap;
  std::map<int, std::string> mStateMap;
  std::vector<std::string> mTextures;
};

extern Game gGame;
#define CONFIG(name, variable, def) \
int& variable = gGame.configs[name] = (int)def;
