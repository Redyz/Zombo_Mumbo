#include <SFML/Audio/Listener.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/OpenGL.hpp>
#include <Thor/Vectors.hpp>
#include <Thor/Math.hpp>
#include <iostream>
#include <string.h>
#include <iomanip>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <math.h>
#include <set>

#include "item.hpp"
#include "game.hpp"
#include "utility.hpp"

#if NUKLEAR_ENABLED
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_SFML_GL2_IMPLEMENTATION
#include "external/nuklear.h"
#include "external/nuklear_sfml_gl2.h"
static struct nk_color background = nk_rgb(28,48,62);
#endif

#define SIZE(thing) sizeof(thing)/sizeof(thing[0])

#define ikp sf::Keyboard::isKeyPressed

#define DEBUG_ONLY(code) do{if(gGame.settings.show_editor){ code; }}while(0)
#define RELEASE_ONLY(code) do{if(!gGame.settings.show_editor){ code; }}while(0)

bool V = false;

#define PARSE(strea) do{ \
  line.erase(0, line.find("=")+1); \
  std::istringstream iss(line); \
  iss >> strea; \
  if(V) std::cout << "Parsed: " << line.c_str() << "\n"; }while(0); \

#define L std::getline(infile, line)

Game gGame;

CONFIG("draw_edges", gEdge, false);
CONFIG("draw_to_edge", gLineToEdge, false);
CONFIG("draw_mouse", gDrawMouse, false);
CONFIG("draw_spawners", gDrawSpawners, true);
CONFIG("draw_lights", gDrawLights, true);
CONFIG("draw_nodes", gDrawNodes, true);
CONFIG("draw_targets", gDrawTargets, true);
CONFIG("draw_shadows", gDrawShadow, true);
CONFIG("draw_shaders", gDrawShaders, true);
CONFIG("godmode", gGodMode, false);

shop_item::shop_item(int p, int a, std::string t, std::function<std::shared_ptr<Item>(void)> creator)
{
  price = p;
  ammo_price = a;
  tag = t;
  create = creator;
}

std::string Line::str()
{
  return SSTR(from.x << " " << from.y << " -> " << to.x << " " << to.y);
}

static std::map<std::string, ENTITY_CREATION> sCreationMap;

Entity::Entity()
{
	id = gGame.get_id();
}

void Entity::kill()
{
  if(group & ENTITY_GROUP_GRENADE)
  {
    for(int i = 0; i < 50; i++)
    {
      v2f direction = v2f(thor::random(-1.0f, 1.0f), thor::random(-1.0f, 1.0f));
      muzzle_location = position;
      gGame.create_projectile(*this, position+direction, PROJECTILE_SHRAPNEL);
    }
	gGame.play_sound("grenade", position);
    gGame.effects.lights.push_back(v3f_c(position));
  }
}

Game::Game()
{
  settings.video_modes = sf::VideoMode::getFullscreenModes();
  settings.current_mode = settings.video_modes.at(0);
  sfWindow.create(sf::VideoMode(settings.current_mode.width, settings.current_mode.height), "ZuMbo MumBo", settings.sf_window_style);
  view = sf::View(sf::FloatRect(0, 0, sfWindow.getSize().x, sfWindow.getSize().y));
  sfui = sf::View(sf::FloatRect(0, 0, sfWindow.getSize().x, sfWindow.getSize().y));

  load_settings();


  sf::Image icon;
  icon.loadFromFile("assets/icon.png");
  sfWindow.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

  renderbuffer.create(1920, 1080);
  sfml.font.loadFromFile("assets/VT323-Regular.ttf");
#if NUKLEAR_ENABLED
  ctx = nk_sfml_init(&sfWindow);
  nk_sfml_font_stash_begin(&atlas);
  nk_sfml_font_stash_end();
#endif
  renderbuffer.setView(view);
  
  std::vector<std::string> texture_strings = get_filenames_in_directory("assets/gfx");

  for(auto texture : texture_strings)
  {
    size_t lastindex = texture.find_last_of(".");
    std::string rawname = texture.substr(0, lastindex);
    textures[rawname].loadFromFile(CSTR("assets/gfx/" << texture));
    std::cout << "Loaded image: " << rawname << " : " << texture << "\n";
    sprites[rawname].setTexture(textures[rawname]);
    mTextures.push_back(rawname);

    if(rawname.find("zombie") != std::string::npos)
    {
      auto strings = split(rawname, '_');
      zombie_names.insert(strings.back());
    }
  }

  std::sort(mTextures.begin(), mTextures.end());
  shadow_shader.loadFromFile("assets/shadows.frag", sf::Shader::Fragment);

  std::vector<std::string> audio_strings = get_filenames_in_directory("assets/sfx");

  for(auto audio : audio_strings)
  {
    size_t lastindex = audio.find_last_of(".");
    std::string rawname = audio.substr(0, lastindex);
    sounds[rawname].loadFromFile(CSTR("assets/sfx/" << audio));
    std::cout << "Loaded sound: " << rawname << " : " << audio << "\n";
  }

  level_names = get_filenames_in_directory("assets/maps");

  for(int i = 0; i < MOUSE_BUTTONS; i++)
  {
    mouse_down[i] = false;
    mouse_last_press[i] = 0;
  }

  predefined_polys.insert(std::make_pair("Crate", create_from_points({
    v2f(0, 0), v2f(50, 0), v2f(50, 50), v2f(0, 50),
  }).set_label("crate")));
  predefined_polys["Crate"].group = POLYGON_GROUP_GLASS;

  predefined_polys.insert(std::make_pair("Shop", create_from_points({
    v2f(0, 0), v2f(20, 0), v2f(20, 20), v2f(0, 20),
  }).set_label("shop")));


  predefined_polys.insert(std::make_pair("Shelf", create_from_points({
    v2f(0, 0), v2f(25, 0), v2f(25, 50), v2f(0, 50),
  }).set_label("wooden_floor")));

  predefined_polys.insert(std::make_pair("Shelf2", create_from_points({
    v2f(0, 0), v2f(50, 0), v2f(50, 25), v2f(0, 25),
  }).set_label("wooden_floor")));

  ambient_track.openFromFile("assets/sfx/ambient.ogg");
  ambient_track.setLoop(true);
  ambient_track.setVolume(150);
  ambient_track.play();
  ending_track.openFromFile("assets/sfx/Delta_Worldwide_Group_-_Talyk_-_So_Good.ogg");
  ending_track.setLoop(true);
  ending_track.setVolume(100);
}

void Game::map_message(std::string mess, sf::Vector2f position, sf::Color color)
{
  message.done = false;
  message.type = ONMAP;
  message.text = mess;
  message.color = color;
  message.lifetime = 500;
  message.position = position;
  message.max_lifetime = message.lifetime;
}

void Game::center_message(std::string mess, sf::Color color, int font_size)
{
  message.done = false;
  message.type = ONSCREEN;
  message.text = mess;
  message.color = color;
  message.lifetime = 500;
  message.font_size = font_size;
  message.max_lifetime = message.lifetime;
}

void Game::bottom_message(std::string mess, sf::Color color, int font_size, sf::Color flashColor, int lifetime)
{
  bottom_message_t.done = false;
  bottom_message_t.text = mess;
  bottom_message_t.color = color;
  bottom_message_t.flash_color = flashColor;
  bottom_message_t.lifetime = lifetime;
  bottom_message_t.font_size = font_size;
  bottom_message_t.max_lifetime = message.lifetime;
}

void Game::play_sound(std::string name, float volume)
{
  play_sound(name, sf::Vector2f(0, 0), volume);
}

void Game::play_sound(std::string sound, sf::Vector2f position, float volume)
{
  sp<sf::Sound> buff = std::make_shared<sf::Sound>();
  buff->setBuffer(sounds[sound]);
  buff->setPitch(thor::random(0.96f, 1.04f));
  if(position == v2f(0, 0))
  {
    position = gGame.player().position;
  }

  buff->setVolume(settings.volume);
  buff->setPosition(position.x, position.y, 0.0f);
  buff->setMinDistance(150.0f);
  buff->setAttenuation(10.f);

  play_queue.push_back(buff);
  play_queue.back()->play();
}

void Game::reset_game(std::string filename)
{
  RELEASE_ONLY(gEdge = false);
  RELEASE_ONLY(gLineToEdge = false);
  RELEASE_ONLY(gDrawMouse = false);
  RELEASE_ONLY(gDrawSpawners = false);
  RELEASE_ONLY(gDrawLights = false);
  RELEASE_ONLY(gDrawNodes = false);
  RELEASE_ONLY(gDrawTargets = false);
  RELEASE_ONLY(gDrawShadow = true);
  RELEASE_ONLY(gDrawShaders = true);
  RELEASE_ONLY(gGodMode = false);

  set_logic(LOGIC_MENU);
  status = RUNNING;
  clear_map();

  bottom_message_t.text = "";
  message.text = "";

  parse_map(filename);

  if(entities.size() == 0)
  {
    // Player
    entities.push_back(Entity());
	effects.lights.push_back(v3f_c(player().position, 0));
  }

  // Commit some settings
  player().position = settings.player_position;
  player().inventory.clear();

  DEBUG_ONLY(player().inventory.push_back(sp<Item>(new Gun())));
  DEBUG_ONLY(player().inventory.push_back(sp<Item>(new UziAkimbo())));
  DEBUG_ONLY(player().inventory.push_back(sp<Item>(new Uzi())));
  DEBUG_ONLY(player().inventory.push_back(sp<Item>(new Shotgun())));
  player().inventory.push_back(sp<Item>(new Glock()));
  player().item = player().inventory.front();
  player().color = sf::Color::Green;
  player().speed = 20.0f;
  player().group = ENTITY_GROUP_BULLET_PROOF | ENTITY_GROUP_PLAYER;
  player().health = player().max_health;
  player().grenades = 2;
  DEBUG_ONLY(player().grenades = 100);
  player().w = player().h = 10;
  camera_pos = player().position;
  editor_actions.clear();
}

void Game::parse_map(std::string filename)
{
  sCreationMap =
  {
    std::make_pair("spawner", ENTITY_SPAWNER),
    std::make_pair("navnode", ENTITY_NAVNODE),
    std::make_pair("light", ENTITY_LIGHT),
  };
  std::ifstream infile(filename);

  std::string line;
  ZPolygon poly;
  unsigned int group;
  std::vector<v2f> points;
  int stage = STAGE_CONFIG;
  std::string label = "concrete", comment = "";
  settings_struct& s = gGame.settings;
  int currentEntity = ENTITY_NONE;
  while (std::getline(infile, line))
  {
    if(line.find("##") != std::string::npos)
      continue;

    if(line.find("====") != std::string::npos)
    {
      stage++;
      continue;
    }
    if(stage == STAGE_CONFIG)
    {
      PARSE(s.player_position.x >> s.player_position.y);L;
      line.erase(0, line.find("=")+1); s.map_description = line; L;
    }
    else if(stage == STAGE_ENTITIES)
    {
      if(line.size() < 2)
      {
        currentEntity = ENTITY_NONE;
      }
      else if(line.find(":") != std::string::npos)
      {
        std::string type = line.substr(0, line.find(":"));
        currentEntity = sCreationMap[type];
      }
      else if(line.find("=") != std::string::npos)
      {
        switch(currentEntity)
        {
          case ENTITY_SPAWNER:
          {
            Spawner sp;
            PARSE(sp.position.x >> sp.position.y);
            spawners.push_back(sp);
            break;
          }
          case ENTITY_NAVNODE:
          {
            Node node;
            PARSE(node.position.x >> node.position.y);
            navnodes.push_back(node);
            break;
          }
          case ENTITY_LIGHT:
          {
            v3f light;
            PARSE(light.x >> light.y >> light.z);
            static_lights.push_back(light);
            break;
          }
          default:
            break;
        }
      }
    }
    else if(stage == STAGE_POLYGONS)
    {
      if(line.size() < 2)
      {
        poly = create_from_points(points);
        poly.texture_label = label;
        poly.group = group;
        poly.comment = comment;
        label = "concrete";
        comment = "";
        group = POLYGON_GROUP_COLLIDE;
        add_action(std::make_shared<editor_action_create>(poly));
        points.clear();
      }
      else if(line.find("=") != std::string::npos)
      {
        PARSE(label);
      }
      else if(line.find("#") != std::string::npos)
      {
        comment = line.substr(1, line.size());
      }
      else if(line.find("+") != std::string::npos)
      {
        PARSE(group);
      }
      else if(line.find(":") == std::string::npos)
      {
        float x1, y1;
        std::istringstream iss(line);
        iss >> x1 >> y1;
        points.push_back(v2f(x1, y1));
      }
    }
  }
}

void Game::destroy()
{
  sfWindow.close();
#if NUKLEAR_ENABLED
  nk_sfml_shutdown();
#endif
}

void Game::next_wave()
{
  current_round++;
  for(int i = 0; i < 5+std::ceil(current_round*2.9f); i++)
  {
    auto& s = spawners.at(rand()%spawners.size());
    create_mob(s.position + v2f(thor::random(-2.0f, 2.0f), thor::random(-2.0, 2.0f)));
  }

  play_sound("round_starts");
  center_message(SSTR("Round " << current_round <<" of " << total_rounds << " starts"));
  rules.next_round_in = 0;
  show_shop = false;
}
void Game::step(float dt)
{
  if((keys_down[sf::Keyboard::G] || mouse_down[sf::Mouse::Right]) && player().grenades > 0)
  {
    if(current_throw_amount > settings.max_throw_duration)
      current_throw_amount = settings.max_throw_duration;
    else
      current_throw_amount += dt;
  }

  ambient_track.setVolume(settings.volume);
  ending_track.setVolume(settings.volume);
  ambient_track.setPosition(v3f_c(player().position));
  static float zoom_in = 0.30f;
  static float zoome = get_zoom();
  if(get_zoom() > zoom_in && !initial_zooming_completed)
  {
    zoom(0.98f);
    if(get_zoom() <= zoom_in+EPSILON)
      initial_zooming_completed = true;
  }

  if(show_shop)
    dt *= 0.5;
  effects.particles_this_frame = 0;
  DEB(1, if(effects.bought_frames > 0) effects.bought_frames--; );
  entities.insert(entities.end(), to_add.begin(), to_add.end());
  to_add.clear();

  std::vector<std::vector<Particle>::iterator> part_to_remove;
  for(auto partIt = particles.begin(); partIt != particles.end(); partIt++)
  {
    Particle& particle = *partIt;
    particle.ttl -= 1;
    particle.position += particle.velocity*dt;
    particle.velocity *= (float)settings.air_friction;
    float mag = magnitude(particle.velocity);
    if(particle.type == PARTICLE_SHOTGUN_CASING || particle.type == PARTICLE_GUN_CASING)
    {
      particle.rotation += 1.0f*mag;
    }
    if(mag < 50.0f)
      particle.velocity = v2f(0, 0);
    if(particle.ttl == 0)
      part_to_remove.push_back(partIt);
  }

  for(auto part : part_to_remove)
    particles.erase(part);

  while(particles.size() > 1000)
    particles.erase(particles.begin());

  for(auto& part : particles)
  {
    bool bounced = false;
    if(part.velocity == v2f(0, 0))
      continue;
    for(auto& obstacle : obstacles)
    {
      if((obstacle.group & POLYGON_GROUP_COMBINE_COLLIDE) == 0 || obstacle.health <= 0)
        continue;

      if(bounced) break;
      for(auto& line : obstacle.lines)
      {
        v2f pos = part.position-closest_point_on_line(part.position, line);
        if(magnitude(pos) < 3)
        {
          v2f norm = thor::unitVector(normal(line.from-line.to));
          if(part.type != PARTICLE_BLOOD && part.type != PARTICLE_BRAIN)
            part.velocity = part.velocity - 2.0f*(thor::dotProduct(part.velocity, norm)*norm);
          else
            part.velocity *= 0.0f;
          if(part.type == PARTICLE_SHOTGUN_CASING || part.type == PARTICLE_GUN_CASING)
          {
            part.velocity *= 0.5f;
            play_sound("shotgunshell");
          }
          else
            part.velocity /= 8.0f;
          bounced = true;
          break;
        }
      }
    }
  }

  if(logic_status == LOGIC_LOST && tick % 180 == 0)
  {
    camera_pos = entities.at(1+rand()%entities.size()-1).position;
    center_message("PRESS R TO RESTART", sf::Color::Yellow, 60);
  }else if(logic_status == LOGIC_WON)
  {
    bottom_message("PRESS R TO RESTART", sf::Color::Yellow, 30, sf::Color::Red);
  }

  for(auto it = obstacles.begin(); it != obstacles.end();)
  {
    if((*it).health < 0)
    {
      ZPolygon& o = *it;
      if(&*it == selected){
        selected = nullptr;
        hovered = nullptr;
      }
      if((o.group & POLYGON_GROUP_COMBINE_REPAIRABLE) == 0)
        it = obstacles.erase(it);
      else
        it++;
    }else
      it++;
  }

  while(!play_queue.empty() && play_queue.at(0)->getStatus() == sf::Sound::Stopped){
    play_queue.erase(play_queue.begin());
  }

  if(is_round_finished())
  {
    if(current_round < total_rounds)
    {
      if(logic_status == LOGIC_ONGOING)
      {
        set_logic(LOGIC_SHOP_STAGE);
      }
      else if(logic_status == LOGIC_SHOP_STAGE && rules.next_round_in < 0)
      {
        set_logic(LOGIC_ONGOING);
        next_wave();
      }
      else
      {
        rules.next_round_in -= dt;
      }
    }
    else if(logic_status == LOGIC_ONGOING)
    {
      set_logic(LOGIC_WON);
    }
  }

  {
	  std::vector<std::vector<Spawner>::iterator> to_remove;
	  for (auto it = spawners.begin(); it != spawners.end(); it++)
	  {
		  if ((*it).spawn_left <= 0)
		  {
			  to_remove.push_back(it);
		  }
	  }

	  for (auto it : to_remove)
		  spawners.erase(it);
  }

  std::remove_if(spawners.begin(), spawners.end(), [=](Spawner& spawner){
    return spawner.spawn_left <= 0;
  });


  static uint target_entity = 0;
  target_entity++;
  target_entity = target_entity % entities.size();

#if 1
  for(uint i = 0; i < entities.size(); i++)
  {
    Entity& e = entities[i];

    if(e.group & ENTITY_GROUP_MOB)
    {

      const float reached_proximity = logic_status == LOGIC_LOST ? 400.0f : 50.0f;
      float target_distance;
      if(tick - e.last_target_update > settings.target_update && i == target_entity)
      {
        v2f desired_position = player().position;

        // Can't see player
        if(!raycast(e.position, target_distance, player().position) || logic_status == LOGIC_LOST)
        {
          std::vector<nav_cost> costs;
          for(auto node : navnodes)
          {
            // Look for visible nodes
            if(raycast(e.position, target_distance, node.position))
            {
              nav_cost cost;
              cost.target = node;
              cost.cost = magnitude(node.position-e.position);
              costs.push_back(cost);
            }
          }

          std::sort(costs.begin(), costs.end(), [=](nav_cost& lhs, nav_cost&rhs)
          {
            return lhs.cost < rhs.cost;
          });

          if(e.target_accumulator > 2.0f)
          {
            e.target_accumulator = 0.0f;
            e.last_position = e.position;
          }

          for(auto costIt = costs.begin(); costIt != costs.end(); costIt++)
          {
            std::vector<nav_cost>::iterator best = costs.begin();
            v2f diff = player().position - best->target.position;
            v2f player_diff = player().position - e.position;
            float diff_mag = magnitude(diff);

            // Already close to the closest - find the next best thing
            if(diff_mag < reached_proximity)
              continue;

            // too far from the closest node
            if(diff_mag > magnitude(player_diff) && logic_status != LOGIC_LOST)
            {
              // we did bad in the last few frames - get a random target
              if(magnitude(e.position-e.last_position) < 3.0f && (e.mob_state == STATE_CHASING_PLAYER || e.mob_state == STATE_CRUISING))
              {
                if(magnitude(best->target.position-e.position) < 5.0f)
                  continue;
                desired_position = costs.at(rand()%costs.size()).target.position;
                e.next_retarget = thor::random(1.0f, 7.0f);
                e.mob_state = STATE_CRUISING;
                break;
              }else
              {
                // keep target
                if(e.next_retarget >= 0 && e.mob_state == STATE_CRUISING)
                {
                  if(magnitude(e.target-e.position) < 5.0f)
                    continue;
                  desired_position = e.target;
                  break;
                }
                else
                {
                  e.mob_state = STATE_CHASING_PLAYER;
                  desired_position = player().position;
                }
              }
            }
            else
            {
              desired_position = best->target.position;
              e.mob_state = STATE_CHASING_NODE;
              break;
            }
          }
        }else
        {
          desired_position = player().position;
          e.mob_state = STATE_CHASING_PLAYER;
        }

        if(logic_status == LOGIC_LOST)
          desired_position = e.target + v2f(-1+rand()%2, -1+rand()%2);
        e.target = desired_position;
        e.last_target_update = tick;
      }

      v2f target = e.target - e.position;
      if(thor::length(target) > EPSILON)
      {
        e.velocity += thor::unitVector(target)*dt*e.speed;
      }
    }

    // Collision vs entity
    for(uint ii = 0; ii < entities.size(); ii++)
    {
      if(ii == i)
        continue; //skip self

      Entity& ee = entities[ii];

      float radius_sum = e.w + ee.w;
      v2f d = ee.position - e.position;
      float l = thor::length(d);

      // intersect
      if(l < radius_sum)
      {
        float intersect = radius_sum - l;
        if(l < EPSILON)
          continue;
        if(((e.group & ENTITY_GROUP_BULLET || e.group & ENTITY_GROUP_GRENADE) && (ee.group & ENTITY_GROUP_BULLET_PROOF)) || ((e.group & ENTITY_GROUP_BULLET_PROOF) && (ee.group & ENTITY_GROUP_GRENADE)))
          continue;

        v2f unit = thor::unitVector(d);
        v2f total_energy = unit * intersect*1.2f;// * settings.collision_entity_bounce_factor;
        ee.velocity += total_energy;
        e.velocity -= total_energy;

        // Zombie vs player..?
        if(e.group & ENTITY_GROUP_MOB && ee.group & ENTITY_GROUP_PLAYER)
        {
          if(!gGodMode)
          {
            if(time.getElapsedTime().asSeconds() - e.last_attack > settings.attack_cooldown)
            {
              ee.health -= 40+rand()%40;
              ee.velocity *= 0.7f;
              e.last_attack = time.getElapsedTime().asSeconds();
              e.velocity = -d*0.5f;
              play_sound("attack", e.position);
              e.anim_frames = 10;
              e.anim_state = thor::random(ANIM_LEFT_ARM, ANIM_RIGHT_ARM);

              for(int i = 0; i < 5; i++)
                create_particle(e.position, e.position+v2f(thor::random(-5, 5), thor::random( -5, 5)), 600.0f, PARTICLE_BLOOD);
            }
          }
        }
      }
    }

    // Collision vs environment
    for(uint ii = 0; ii < obstacles.size(); ii++)
    {
      auto& o = obstacles[ii];
      if(((&e == &player() && (o.group & POLYGON_GROUP_PLAYER_ONLY) != 0) || (o.group & POLYGON_GROUP_COMBINE_COLLIDE) != 0) && o.health > 0)
      {
        for(auto line : obstacles[ii].lines)
        {
          v2f p = closest_point_on_line(e.position, line);
          v2f d = e.position - p;
          float l = thor::length(d);
          if(l < EPSILON || l > e.w || p.x != p.x || p.y != p.y)
            continue;
          v2f unit = thor::unitVector(d);
          float intersect = e.w - l;
          if(e.group & ENTITY_GROUP_GRENADE)
          {
            v2f direction = e.position - p;
            v2f norm = Math::norm(line.to-line.from);
            if(norm == v2f(0, 0))
              continue;
            norm = thor::unitVector(norm);
            v2f mirror = direction - 2*(thor::dotProduct(direction, norm))*norm;
//             play_sound("wallhit", e.position);
            e.velocity += mirror*0.5f;
          }
          else
          {
            e.position += unit*intersect;
          }

          if((o.group & POLYGON_GROUP_COMBINE_DESTRUCTIBLE) != 0 && e.group & ENTITY_GROUP_MOB)
          {
            if(time.getElapsedTime().asSeconds() - e.last_attack > settings.attack_cooldown)
            {
              e.last_attack = time.getElapsedTime().asSeconds();
              if(o.health > 0)
              {
                o.health -= 4+rand()%20;
                play_sound("wallhit", e.position);
                e.anim_frames = 10;
                e.anim_state = thor::random(ANIM_LEFT_ARM, ANIM_RIGHT_ARM);
                if(o.health <= 0)
                {
                  if(o.group & POLYGON_GROUP_GLASS)
                    play_sound("glass", e.position);
                  else if(o.group & POLYGON_GROUP_DOOR_CLOSED)
                    play_sound("door_break", e.position);
                }
              }
            }
          }
        }
      }
    }

    // Velocity
    entity_step(e, dt);
  }

#endif
  sf::Listener::setPosition(player().position.x, player().position.y, 0.0f);

  if(get_mouse_pos()-player().position != v2f(0, 0))
  {
    v2f targ = thor::unitVector(get_mouse_pos()-player().position);
    sf::Listener::setDirection(targ.x, targ.y, 0.0f);
  }
  // Remove temporaries
  {
	std::vector<int> to_remove;
    for(auto entIt = entities.begin(); entIt < entities.end(); entIt++)
    {
      Entity& e = *entIt;

      // Dead entity (time)
      if(e.group & ENTITY_GROUP_TEMP && time.getElapsedTime().asSeconds() - e.birth > TEMP_LIFETIME)
      {
        e.health = -1;
        to_remove.push_back(e.id);
      }
      else if((e.group & ENTITY_GROUP_BULLET && std::abs(e.velocity.x)+std::abs(e.velocity.y) < 2))
        to_remove.push_back(e.id);
      else
      {
        // Bullet vs others
        for(auto otherEntIt = entities.begin(); otherEntIt < entities.end(); otherEntIt++)
        {
          Entity& ee = *otherEntIt;
          if(otherEntIt == entIt)
            continue;

          if(e.group & ENTITY_GROUP_BULLET && circle_collision(e, ee) && !(ee.group & ENTITY_GROUP_BULLET_PROOF)
            && !(ee.group & ENTITY_GROUP_BULLET))
          {
            ee.health -= 100;
            play_sound("hit", ee.position);
            e.health = -1;
            to_remove.push_back(e.id);

            v2f direction = ee.position - e.position;
            direction = thor::unitVector(direction);
            ee.velocity += direction*settings.knockback;

            v2f pdirection = ee.position-player().position;

            if(pdirection == v2f(0, 0))
              continue;

            v2f dir = Math::norm(ee.position - player().position) * 5.0f;
            for(int i = 0; i < 60; i++)
            {
              create_particle(e.position+v2f(1.0f/(rand()%100), 1.0f/(rand()%100))+v2f(-5+rand()%10,-5+rand()%10), ee.position+dir, 500.0f, PARTICLE_BLOOD);
            }

            for(int i = 0; i < 5; i++)
            {
              create_particle(e.position+v2f(1.0f/(rand()%100), 1.0f/(rand()%100))+v2f(-5+rand()%10,-5+rand()%10), ee.position+dir, 500.0f, PARTICLE_BRAIN);
            }
          }
        }
      }

      if(e.health < 0)
      {
        e.health = 0;
        e.kill();
        if(e.group & ENTITY_GROUP_MOB){
          zombies_killed++;
          zombies_left--;
          play_sound("death", e.position);
          score+=5+rand()%12;
        }

        to_remove.push_back(e.id);
      }
    }

    for(auto currentId : to_remove)
    {
	  auto theIt = std::find_if(entities.begin(), entities.end(), [=](Entity& entity) { return entity.id == currentId; });
      if(theIt != entities.end() && &*theIt == &player())
      {
        set_logic(LOGIC_LOST);
        center_message("YOU BECAME ONE OF THEM", sf::Color::Red, 60);
      }

	  if(theIt != entities.end())
        entities.erase(theIt);
    }

    while(decals.size() > settings.max_decals)
      decals.erase(decals.begin());
  }

  tick++;
}

void Game::reset_window()
{
}

void Game::create_grenade(Entity& parent, sf::Vector2f target, float speed)
{
  Entity gren;
  gren.w = gren.h = 2.0f;
  gren.color = sf::Color::Black;

  if(target == parent.position)
    return;

  v2f direction = thor::unitVector(target-parent.position);
  gren.velocity = direction*speed;
  gren.position = parent.position+direction*10.0f;
  gren.muzzle_location = gren.position;

  gren.birth = time.getElapsedTime().asSeconds();
  gren.group = ENTITY_GROUP_TEMP | ENTITY_GROUP_GRENADE | ENTITY_GROUP_BULLET_PROOF;
  to_add.push_back(gren);
}

void Game::create_particle(sf::Vector2f start, sf::Vector2f target, float speed, int type)
{
  Particle particle;
  particle.position = start;
  particle.ttl = -1;
  if(start == target)
    return;
  effects.particles_this_frame++;
  if(effects.particles_this_frame > effects.max_particles_this_frame)
    return;
  particle.velocity = Math::norm(target-start)*(thor::random(speed/2.0f, speed));
  particle.type = type;
  particle.rotation = rand()%360;
  switch(type)
  {
    case PARTICLE_BLOOD:
      particle.color = sf::Color(255, 0, 0, 100+rand()%155);
      particle.size = thor::random(0.1f, 4.0f);
      break;
    case PARTICLE_BRAIN:
      particle.color = sf::Color(255, 20, 147);
      particle.size = thor::random(0.1f, 1.0f);
      break;
    case PARTICLE_SHOTGUN_CASING:
    case PARTICLE_GUN_CASING:
      particle.size = 0.1f;
      break;
  }
  particles.push_back(particle);
}

void Game::create_projectile(Entity& parent, sf::Vector2f target, int projectile_type)
{
  //effects.flash_frames = 1;

  Entity bullet;

  if(target == parent.position)
    return;

  v2f dontcare;
  if(!raycast(parent.position, dontcare, parent.muzzle_location, POLYGON_GROUP_COLLIDE))
    return;

  bullet.speed = 30;
  bullet.w = bullet.h = 0.25f;
  if(projectile_type == PROJECTILE_SHRAPNEL)
    bullet.w = bullet.h = thor::random(0.05f, 0.3f);

  bullet.color = sf::Color::Yellow;
  bullet.extra_group = projectile_type;
  v2f direction = thor::unitVector(target-parent.position);
  bullet.velocity = direction*bullet.speed;
  bullet.position = parent.muzzle_location+direction;
  bullet.birth = time.getElapsedTime().asSeconds();
  bullet.group = ENTITY_GROUP_BULLET | ENTITY_GROUP_TEMP;
  bullet.rotation = Math::get_angle_between_coordinates(parent.position, target);
  to_add.push_back(bullet);

  bullets_fired++;
}

void Game::create_decal(sf::Vector2f where, float rotation, std::string label)
{
  Entity decal;
  decal.position = where;
  decal.rotation = rotation;
  decal.decal = label;
  decal.birth = time.getElapsedTime().asSeconds();
  decal.group = ENTITY_GROUP_DECAL | ENTITY_GROUP_TEMP;
  decals.push_back(decal);
}

void Game::create_mob(sf::Vector2f where)
{
  Entity e;
  e.position = where;
  e.rotation = rand()%360;
  e.speed = thor::random(18.0f, 21.0f);
  e.base_sprite = *std::next(zombie_names.begin(), rand()%zombie_names.size());
  zombies_left++;
  to_add.push_back(e);
}

void Game::create_spawner(sf::Vector2f where)
{
  Spawner e;
  e.position = where;
  e.last_spawn = tick;
  e.spawn_left = 3;
  spawners.push_back(e);
}

void Game::create_navnode(sf::Vector2f where)
{
  Node nav;
  nav.position = where;
  navnodes.push_back(nav);
}

void Game::draw_blood(Entity& e)
{
  v2f p = e.position;
  float ang = Math::get_angle_between_coordinates(p.x, p.y, e.target.x, e.target.y)-180;
  std::string decal = "mob";
  int level = 0;
  if(e.health < 200)
    level = 3;
  else if(e.health < 299)
    level = 2;

  if(level > 0)
  {
    decal = SSTR(decal << level);
    draw_sprite(e.position.x, e.position.y, e.position.x+2*e.w, e.position.y+2*e.w, decal, ang);
  }
}

void Game::entity_step(Entity& e, float dt)
{
  v2f old_pos = e.position;
  if(e.mob_state == STATE_CHASING_PLAYER)
    e.next_retarget -= dt;

  if(magnitude(e.velocity) > settings.max_entity_velocity && e.velocity != v2f(0, 0))
    e.velocity = thor::unitVector(e.velocity) * settings.max_entity_velocity;

  float speed = e.speed;
    if(e.running && e.stamina >= 2)
    {
      speed *= 1.8f;
      DEB(1, e.stamina -= 2;);
    }

  e.position += e.velocity*speed*dt;
  e.target_accumulator += dt;

  if(e.last_noise <= -1)
    e.last_noise = 100+rand()%settings.moan_frequency;
  DEB(1,
    e.last_noise--;
  );

  if(e.group & ENTITY_GROUP_GRENADE)
  {
    e.rotation += dt*magnitude(e.velocity)*180.0f;
    e.rotation = std::fmod(e.rotation, 360.0f);
    effects.lights.push_back(v3f_c(e.position, 1.0f));
  }

  if(e.group & ENTITY_GROUP_MOB)
  {
    if(e.last_noise == 0)
      play_sound("zombie", e.position);

    DEB(1,
    if(e.anim_frames > 0)
      DEB(1, e.anim_frames--;);
    );

    if(e.anim_frames == 0)
    {
      switch(e.anim_state)
      {
        case ANIM_LEFT_ARM:
        case ANIM_RIGHT_ARM:
        {
          e.anim_state = ANIM_BOTH_ARMS;
          e.anim_frames = -1;
        }
        break;
      }
    }
  }

  Line line(old_pos, e.position);
//   draw_line(line.from.x, line.from.y, line.to.x, line.to.y, sf::Color::Blue);
  for(auto& polygon : obstacles)
  {
    for(auto& other : polygon.lines)
    {
      if(line_collision(line, other) && polygon.group & POLYGON_GROUP_COLLIDE)
      {
        if(e.group & ENTITY_GROUP_BULLET && polygon.group & POLYGON_GROUP_GLASS)
          continue;

        if(e.group & ENTITY_GROUP_BULLET)
          e.velocity = v2f(0, 0);
        else
        {
          v2f direction = e.position - old_pos;
          if(direction == v2f(0, 0))
            continue;
          v2f norm = Math::norm(line.to-line.from);
          if(norm == v2f(0, 0))
            continue;
          norm = thor::unitVector(norm);
          v2f mirror = direction - 2*(thor::dotProduct(direction, norm))*norm;
//           play_sound("wallhit", e.position);
          e.position = old_pos+mirror;
          e.velocity += mirror*0.5f;
        }
        break;
      }
    }
  }

  if(!(e.group & ENTITY_GROUP_BULLET))
  {
    if(e.group & ENTITY_GROUP_GRENADE)
    {
      e.velocity *= 1.0f-(1.0f/settings.air_friction)*dt;
    }
    else
    {
      e.velocity *= 1.0f-(1.0f/settings.friction)*dt;
    }
  }

  if(std::abs(e.velocity.x) < EPSILON) e.velocity.x = 0;
  if(std::abs(e.velocity.y) < EPSILON) e.velocity.y = 0;

  DEB(2,
    if(!e.running)
      e.stamina++;
  );
  if(e.stamina > e.max_stamina)
    e.stamina = e.max_stamina;
}

void Game::save_settings()
{
  std::cout << "Saving configuration to " << get_settings_filepath() << std::endl;

  std::ofstream settings_file(get_settings_filepath());
  settings_file << "vsync=" << settings.v_sync << "\n"
                << "volume=" << settings.volume << "\n"
				<< "winstate=" << settings.sf_window_style << "\n"
				<< "winres=" << settings.selected_mode_index << "\n"
                << "editor=" << settings.show_editor << "\n";

  settings_file.close();
}

void Game::load_settings()
{
  try{

    std::ifstream infile(get_settings_filepath());
    std::string line;
    L;
    PARSE(settings.v_sync);L;
    PARSE(settings.volume);L;
	PARSE(settings.sf_window_style); L;
	PARSE(settings.selected_mode_index); L;
    PARSE(settings.show_editor);L;
  }catch(...)
  {
    LERROR("Could not read config file successfully, defaulting to default values");
  }
}


void Game::save_map(std::string filename)
{
  std::ofstream file(filename);
  file << "player_pos=" << settings.player_position.x << " " << settings.player_position.y << "\n"
       << "description=" << settings.map_description << "\n" // required for next parsing stage
       << "\n" // required for next parsing stage
       << "====\n";

  for(auto spawner : spawners)
  {
    file << "spawner:\n" << "=" << spawner.position.x << " " << spawner.position.y << "\n\n";
  }

  for(auto light : static_lights)
  {
    file << "light:\n" << "=" << light.x << " " << light.y << " " << light.z << "\n\n";
  }

  for(auto node : navnodes)
  {
    file << "navnode:\n" << "=" << node.position.x << " " << node.position.y << "\n\n";
  }

  file << "====\n";
  for(auto poly : obstacles)
  {
    file << "Autogend:\n";
    if(!poly.comment.empty()) file << "#" << poly.comment << "\n";
    file << "=" << poly.texture_label << "\n";
    file << "+" << poly.group << "\n";
    for(auto line : poly.lines)
    {
      file << line.from.x << " " << line.from.y << "\n";
    }
    file << "\n";
  }
  file.close();
}

void Game::clear_map()
{
  bullets_fired = zombies_killed = 0;
  zombies_left = 0;
  score = 30;
  DEBUG_ONLY(score = 500);
  if(entities.size() > 1)
    entities.erase(entities.begin()+1, entities.end());
  spawners.clear();
  decals.clear();
  items.clear();
  navnodes.clear();
  static_lights.clear();
  obstacles.clear();
  particles.clear();
  total_rounds = 7;
  current_round = 0;
  initial_menu_shown = false;
  selected = nullptr;
  hovered = nullptr;
}

Entity & Game::player()
{
  return entities[0];
}

bool Game::is_round_finished()
{
  if(entities.size() == 1)
    return true;
  return false;
}

void Game::set_logic(GAME_LOGIC_STATUS new_logic)
{
  bool changed = logic_status != new_logic;
  if(logic_status != LOGIC_MENU)
    previous_logic_status = logic_status;
  logic_status = new_logic;
  if(changed)
  {
    switch(logic_status)
    {
      case LOGIC_SHOP_STAGE:
      {
        rules.next_round_in = rules.max_next_round_in;
        play_sound("shop_stage");
        center_message("Shop stage");
        break;
      }
      case LOGIC_WON:
      {
        player().inventory.push_back(sp<Item>(new Pizza()));
        player().item = player().inventory.back();
        ambient_track.pause();
        ending_track.play();
        center_message("GAME OVER - YOU ARE NOW INFECTED... WITH RHYTHM!", sf::Color::Yellow, 40);
        play_sound("game_won");
        break;
      }
      default:
        break;
    };
  }
}

#define DDEB(time, code) code;

bool Game::is_editor_click()
{
  if(!is_in_editor())
    return false;
  bool in_editor = nk_window_is_any_hovered(ctx);
  in_editor |= v2f(sf::Mouse::getPosition(sfWindow)).y > (sfWindow.getSize().y-BOTTOM_PANEL_HEIGHT);
  return in_editor;
}

void Game::input(float dt)
{
  sf::Event event;

  bool nuklear_clicked = false;
  #if NUKLEAR_ENABLED
  nk_input_begin(ctx);
  nuklear_clicked = nk_window_is_any_hovered(ctx);
  #endif
  
  for(int i = 0; i < 255; i++)
    keys_down_last[i] = keys_down[i];
  
  for(int i = 0; i < MOUSE_BUTTONS; i++)
    mouse_down_last[i] = mouse_down[i];
  
  while (sfWindow.pollEvent(event))
  {
    if(event.type == sf::Event::Closed)
      status = QUITTING;
    else if(event.type == sf::Event::MouseWheelScrolled)
    {
      if(is_in_editor() && !is_editor_click())
      {
        int current = std::find(mTextures.begin(), mTextures.end(), ed_poly_texture) - mTextures.begin();
        if(event.mouseWheelScroll.delta > 0)
          current += 1;
        else
          current -= 1;

        ed_poly_texture = mTextures.at(current < 0 ? mTextures.size()-1 : current%mTextures.size());
        ed_poly_texture_hovered = ed_poly_texture;
        if(selected)
          selected->texture_label = ed_poly_texture;
      }
      else
      {
        if(player().inventory.size() > 1)
        {
          auto& in = player().inventory;
          int current = std::find(in.begin(), in.end(), player().item) - in.begin();

          if(event.mouseWheelScroll.delta > 0)
            current += 1;
          else
            current -= 1;

          player().item = in.at(current < 0 ? in.size()-1 : current%in.size());
        }
      }
      play_sound("switch");
    }
    else if(event.type == sf::Event::MouseButtonPressed)
      mouse_down[event.mouseButton.button] = true;
    else if(event.type == sf::Event::MouseButtonReleased)
    {
      mouse_down[event.mouseButton.button] = false;
      switch(event.mouseButton.button)
      {
        case sf::Mouse::Left:
        {
#if NUKLEAR_ENABLED
          if(hovered && !is_editor_click())
          {
            selected = hovered;
            ed_poly_texture = selected->texture_label;
            ed_poly_texture_hovered = ed_poly_texture;
          }
#endif
        }
          break;
        //case sf::Mouse::Right: input->handle_click(CLICKTYPE_RIGHT, mouse); break;
        //case sf::Mouse::Middle: input->handle_click(CLICKTYPE_WHEEL, mouse); break;
        default: break;
      }
    }
    else if(event.type == sf::Event::KeyPressed)
    {
      key_modifiers[SHIFT] = event.key.shift;
      key_modifiers[CTRL] = event.key.control;
      key_modifiers[ALT] = event.key.alt;
      keys_down[event.key.code] = true;
    }
    else if(event.type == sf::Event::KeyReleased)
    {
      keys_down[event.key.code] = false;
    }
    else if(event.type == sf::Event::Resized)
    {
      view = sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height));
      sfui = sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height));
      sfWindow.setView(view);
      renderbuffer.create(event.size.width, event.size.height);
      view.setCenter(camera_pos.x, camera_pos.y);
      if(initial_zooming_completed)
        set_zoom(0.32f);
    }
#if NUKLEAR_ENABLED
    nk_sfml_handle_event(&event);
#endif
  }
  
  #if NUKLEAR_ENABLED
  nk_input_end(ctx);
  nuklear_clicked = nk_window_is_any_hovered(ctx);
  #endif
  
  
  if(!sfWindow.hasFocus())
    return;

  sfWindow.setVerticalSyncEnabled(settings.v_sync);

  if(!nuklear_clicked)
  {
    if(mouse_down[sf::Mouse::Left])
    {
      if(status == RUNNING && (logic_status == LOGIC_ONGOING || logic_status == LOGIC_SHOP_STAGE || logic_status == LOGIC_WON))
      {
        if(player().item)
        {
          bool used = player().item->use(player(), dt);
          if(player().item->consume && used)
          {
            player().inventory.erase(std::remove(player().inventory.begin(), player().inventory.end(), player().item));
            player().item = player().inventory.size() > 0 ? player().inventory.at(0) : nullptr;
          }
          mouse_last_press[sf::Mouse::Left] = tick;
        }
      }
    }


    if(ikp(sf::Keyboard::Escape))
    {
      DEB(20,
          if(is_in_editor())
          {
            selected = nullptr;
            hovered = nullptr;
          }
          else if(initial_menu_shown)
          {
            if(!show_shop)
            {
              if(status == RUNNING)
              {
                set_logic(LOGIC_MENU);
                status = PAUSED;
              }
              else if(status == PAUSED && logic_status != LOGIC_MENU)
              {
                status = RUNNING;
                message.lifetime = 0;
                message.text = "";
                effects.pause_menu_flash = 0;
              }
              else
              {
                logic_status = previous_logic_status;
                status = RUNNING;
              }
            }
          }
      );
    }

    bool found = false;
    if(settings.show_editor)
    {
      if(status == PAUSED)
      {
        for(auto& polygon : obstacles)
        {
          if(found)
            break;
          if(polygon.in_poly(get_mouse_pos()))
          {
            v2f diff = get_mouse_pos()-polygon.get_centroid();
            if(magnitude(diff) < 50)
            {
              found = true;
              hovered = &polygon;
              break;
            }
          }
        }
      }

      if(mouse_down[sf::Mouse::Right] && is_in_editor() && ed_mode == EDITOR_MODE_POLYGON && !key_modifiers[CTRL])
      {
        DEB(3,
            if(selected)
              add_action(std::make_shared<editor_action_move>(selected, v2f(get_mouse_pos()-selected->get_centroid())));
        );
      }
    }
  }

  v2f velocity;
  static float shop_debounce = 0;
  const float SHOP_DEB = 15.0f*FPS_TICK;

  if(ikp(sf::Keyboard::P) && !nk_window_is_any_hovered(ctx))
  {
    DEB(15,
        if(status == RUNNING && !show_shop){
          status = PAUSED;
        }else if(status == PAUSED){
          message.lifetime = 0;
          message.text = "";
          effects.pause_menu_flash = 0;
          status = RUNNING;
        }
    )
  }

  if(!nuklear_clicked)
  {
    if(logic_status != LOGIC_LOST)
    {
      if(show_shop || logic_status == LOGIC_MENU)
      {
        if(ikp(sf::Keyboard::Up) || ikp(sf::Keyboard::W)) { VDEB(6, menu_selection -= 1; play_sound("select");); }
        if(ikp(sf::Keyboard::Down) || ikp(sf::Keyboard::S)) { VDEB(6, menu_selection += 1; play_sound("select");); }
        if(ikp(sf::Keyboard::Right) || ikp(sf::Keyboard::D)) { VDEB(menu_items[menu_selection].scroll_speed, menu_right_requested = true; VDEB(menu_items[menu_selection].sound_speed, play_sound("select"));); }
        if(ikp(sf::Keyboard::Left) || ikp(sf::Keyboard::A)) { VDEB(menu_items[menu_selection].scroll_speed, menu_left_requested = true; VDEB(menu_items[menu_selection].sound_speed, play_sound("select"));); }

        menu_selection = menu_selection < 0 ? menu_size : menu_selection;
        menu_selection = menu_selection > (int)menu_size ? 0 : menu_selection;
      }


      if(logic_status == LOGIC_MENU)
      {
        if(ikp(sf::Keyboard::E) || ikp(sf::Keyboard::Return))
        {
          DEB(20,
              int i = 0;
              for(auto pair : menu_items)
              {
                if(i == menu_selection)
                {
                  play_sound("buy");
                  menu_items[i].action();
                  break;
                }
                i++;
              }
          );
        }
        if(ikp(sf::Keyboard::D) || ikp(sf::Keyboard::A) || ikp(sf::Keyboard::Left) || ikp(sf::Keyboard::Right))
        {
          DEB(0,
              int i = 0;
              for(auto pair : menu_items)
              {
                if(i == menu_selection)
                {
                  if(menu_right_requested)
                  {
                    menu_items[i].right_action();
                    menu_right_requested = false;
                  }
                  if(menu_left_requested)
                  {
                    menu_items[i].left_action();
                    menu_left_requested = false;
                  }
                  break;
                }
                i++;
              }
          );
        }
      }


      if(status != PAUSED)
      {
        if(ikp(sf::Keyboard::Return) || ikp(sf::Keyboard::E))
        {
          if(show_shop && (status != PAUSED))
          {
            const int BOUGHT_FRAMES = 10;
            bool bought = false;
            if(time.getElapsedTime().asSeconds() - shop_debounce > SHOP_DEB)
            {
              shop_debounce = time.getElapsedTime().asSeconds() ;
              if(menu_selection == (int)shop_items.size()) //exit
              {
                menu_selection = 0;
                show_shop = false;
                bought = true;
              }
              else if(shop_items[menu_selection].tag == "GRENADE") // lazy
              {
                if(score >= shop_items[menu_selection].price)
                {
                  score -= shop_items[menu_selection].price;
                  player().grenades+=1;
                  bought = true;
                }
              }
              else
              {
                auto& item = shop_items[menu_selection];
                if(score >= item.price)
                {
                  score -= item.price;
                  auto temp = item.create();

                  bool has_it = false;
                  sp<Item> found = nullptr;
                  for(auto& item_pt : player().inventory)
                  {
                    if(item_pt->get_type() == temp->get_type())
                    {
                      has_it = true;
                      found = item_pt;
                      if(item_pt->get_type() == ITEM_TYPE_GROUP_MULTIPLE)
                      {
                        bought = true;
                        player().inventory.push_back(temp);
                        break;;
                      }
                    }
                  }
                  if(!has_it)
                  {
                    player().inventory.push_back(temp);
                    bought = true;
                  }
                  else
                  {
                    auto gun = std::dynamic_pointer_cast<Gun>(found);
                    if(gun)
                    {
                      bought = true;
                      gun->ammo += gun->get_clip_size();
                    }
                  }
                }
              }
              if(bought)
              {
                effects.bought_frames = BOUGHT_FRAMES;
                play_sound("buy");
              }
              else
              {
                effects.bought_frames = BOUGHT_FRAMES;
                play_sound("error");
                bottom_message("CAN'T AFFORD ITEM", sf::Color::Yellow, 30, sf::Color::Red);
              }
            }
          }
        }
        if(!show_shop && logic_status != LOGIC_MENU)
        {
          player().running = ikp(sf::Keyboard::LShift);
          if(ikp(sf::Keyboard::Left) || ikp(sf::Keyboard::A)) velocity.x = -1;
          if(ikp(sf::Keyboard::Right) || ikp(sf::Keyboard::D)) velocity.x = 1;
          if(ikp(sf::Keyboard::Up) || ikp(sf::Keyboard::W)) velocity.y = -1;
          if(ikp(sf::Keyboard::Down) ||ikp(sf::Keyboard::S)) velocity.y = 1;
        }

        auto back = player().velocity;
        velocity *= player().speed*dt;
        player().velocity += velocity;

        if(back != player().velocity)
        {
          if(get_mouse_pos()!=player().position)
          {
            v2f dir = thor::unitVector(get_mouse_pos()-player().position)*20.0f;
            ZPolygon touch;
            bool found = false;
            if(!raycast(player().position, touch, player().position+dir, (POLYGON_GROUP_COMBINE_REPAIRABLE | POLYGON_GROUP_SHOP)))
            {
              interact_poly = touch;
              found = true;
            }

            if(!found)
            {
              interact_poly = INVALID_POLY;
              //bottom_message_t.text = "";
              //             bottom_message("");
            }
            else
            {
              std::string mess = "PRESS E TO ";
              if(touch.group & POLYGON_GROUP_SHOP)
              {
                if(logic_status == LOGIC_SHOP_STAGE)
                  mess += "OPEN THE SHOP";
                else
                  mess = "SHOP IS CURRENTLY CLOSED";
              }
              else if(touch.health <= 0)
                mess += SSTR("REPAIR (" << settings.repair_cost << " POINTS)");
              else if(touch.group & POLYGON_GROUP_DOOR_CLOSED)
                mess += "OPEN THE DOOR";
              else if(touch.group & POLYGON_GROUP_DOOR_OPEN)
                mess += "CLOSE THE DOOR";
              else
                mess = "";
              bottom_message(mess);
              //bottom_message_t.text = mess;
            }
          }
        }
      }else
      {
        if(selected && ed_mode == EDITOR_MODE_POLYGON)
        {

          if(key_modifiers[SHIFT])
          {
            DEB(3, if(ikp(sf::Keyboard::Q)) { add_action(std::make_shared<editor_action_scale>(selected, v2f(ed_move_factor, ed_move_factor)));} )
            DEB(3, if(ikp(sf::Keyboard::E)) { add_action(std::make_shared<editor_action_scale>(selected, v2f(1.0f/ed_move_factor, 1.0f/ed_move_factor)));} )

            DEB(3, if(ikp(sf::Keyboard::Left) || ikp(sf::Keyboard::A)) { add_action(std::make_shared<editor_action_scale>(selected, v2f(ed_move_factor, 1.0f))); } )
            DEB(3, if(ikp(sf::Keyboard::Right) || ikp(sf::Keyboard::D)) { add_action(std::make_shared<editor_action_scale>(selected, v2f(1.0f/ed_move_factor, 1.0f))); } )
            DEB(3, if(ikp(sf::Keyboard::Up) || ikp(sf::Keyboard::W)) { add_action(std::make_shared<editor_action_scale>(selected, v2f(1.0f, ed_move_factor))); } )
            DEB(3, if(ikp(sf::Keyboard::Down) || (ikp(sf::Keyboard::S) && !key_modifiers[CTRL])) { add_action(std::make_shared<editor_action_scale>(selected, v2f(1.0f, 1.0f/ed_move_factor))); } )

          }
          else
          {
            DEB(3, if(ikp(sf::Keyboard::Left) || ikp(sf::Keyboard::A)) { add_action(std::make_shared<editor_action_move>(selected, v2f(-ed_move_factor, 0))); } )
            DEB(3, if(ikp(sf::Keyboard::Right) || ikp(sf::Keyboard::D)) { add_action(std::make_shared<editor_action_move>(selected, v2f(ed_move_factor, 0))); } )
            DEB(3, if(ikp(sf::Keyboard::Up) || ikp(sf::Keyboard::W)) { add_action(std::make_shared<editor_action_move>(selected, v2f(0, -ed_move_factor))); } )
            DEB(3, if(ikp(sf::Keyboard::Down) || (ikp(sf::Keyboard::S) && !key_modifiers[CTRL])) { add_action(std::make_shared<editor_action_move>(selected, v2f(0, ed_move_factor))); } )

            DEB(3, if(ikp(sf::Keyboard::Q)) {
              add_action(std::make_shared<editor_action_rotate>(selected, rad(ed_rotation_factor)));
            } )
            DEB(3, if(ikp(sf::Keyboard::E)) {
              add_action(std::make_shared<editor_action_rotate>(selected, -rad(ed_rotation_factor)));
            } )
          }
        }
      }
    }


    if(key_modifiers[SHIFT])
    {
      if(ikp(sf::Keyboard::Subtract)){ DEB(1, ed_move_factor -= 0.1; ed_move_factor = ed_move_factor < 0 ? 0.0f : ed_move_factor; ) };
      
      if(ikp(sf::Keyboard::Add)){ DEB(1, ed_move_factor += 0.1; ed_move_factor = std::fmod(ed_move_factor, MAX_EDITOR_MOVE_FACTOR); ) };
    }
    else
    {
      if(ikp(sf::Keyboard::Subtract)){ DEB(1, if(initial_zooming_completed) zoom(1.05f)); };
      if(ikp(sf::Keyboard::Add)){ 
        DEB(1, if(initial_zooming_completed) zoom(0.95f)); 
        
      };
    }

    DEBUG_ONLY(
      if(ikp(sf::Keyboard::Space))
      {
        if(status == PAUSED)
        {
          DEB(5,
              if(ikp(sf::Keyboard::Space))
              {
                if(selected)
                {
                  ZPolygon copy = *selected;
                  float offset = (float)(3+thor::random(1, 6));
                  copy.move({offset, offset});
                  add_action(std::make_shared<editor_action_create>(copy));
                  selected = &obstacles.back();
                }
              }
          );
        }
        else
        {
          DEB(1,
            if(spawners.size() > 0)
            {
              create_mob(spawners.at(rand()%spawners.size()).position);
            }
          );

        }
      }
    );



	if (ikp(sf::Keyboard::Q) && (key_modifiers[CTRL] && status != PAUSED && logic_status != LOGIC_LOST )) { logic_status = LOGIC_MENU; menu_status = MENU_QUIT; };

    if(ikp(sf::Keyboard::Z))
    {
      DEB(5,
          if(editor_actions.size() > 0)
          {
            editor_actions.back()->editor_undo();
            editor_actions.erase(editor_actions.end());
          }
      )
    }

    if(ikp(sf::Keyboard::Delete))
    {
      DEB(5,
          for(auto it = obstacles.begin(); it != obstacles.end(); it++)
          {
            if(&*it == selected)
            {
              selected = nullptr;
              hovered = nullptr;
              obstacles.erase(it);
              break;
            }
          }
      );
    }

    if(ikp(sf::Keyboard::Escape))
    {
      if(status == PAUSED)
        ed_points.clear();
    }

    if(ikp(sf::Keyboard::R))
    {
      DEB(5,
          if(status == RUNNING)
          {
            reset_game(SSTR("assets/maps/" << level_names[selected_level]));
            logic_status = LOGIC_ONGOING;
            initial_menu_shown = true;
            ambient_track.play();
            ending_track.pause();
          }
          else if(status == PAUSED)
          {
            if(ed_points.size() > 0)
            {
              ZPolygon poly = create_from_points(ed_points);
              ed_points.clear();
              poly.group = 1 << (1+ed_poly_group);
              switch(poly.group)
              {
                case POLYGON_GROUP_DOOR_OPEN:
                case POLYGON_GROUP_DOOR_CLOSED:
                  poly.texture_label = "door";
                  break;
                case POLYGON_GROUP_SHOP:
                  poly.texture_label = "metal";
                  break;
                case POLYGON_GROUP_GLASS:
                  poly.texture_label = "glass";
                  break;
                case POLYGON_GROUP_SANDBAG:
                  poly.texture_label = "sandbag";
                  break;
                default:
                  poly.texture_label = "concrete";
                  break;
              }
              add_action(std::make_shared<editor_action_create>(poly));
            }
          }
      )
    }
    if(ikp(sf::Keyboard::Numpad5)) settings.lock_camera_to_player = !settings.lock_camera_to_player;

    if(!settings.lock_camera_to_player)
    {
      v2f velocity;
      if(ikp(sf::Keyboard::Numpad4)) velocity.x = -1;
      if(ikp(sf::Keyboard::Numpad6)) velocity.x = 1;
      if(ikp(sf::Keyboard::Numpad8)) velocity.y = -1;
      if(ikp(sf::Keyboard::Numpad2)) velocity.y = 1;

      velocity *= player().speed;
      camera_pos += velocity;
    }


    std::cout << keys_down[sf::Keyboard::G] << " " << keys_down_last[sf::Keyboard::G] << std::endl;
    if(status != PAUSED && ((!keys_down[sf::Keyboard::G] && keys_down_last[sf::Keyboard::G]) || (!mouse_down[sf::Mouse::Right] && mouse_down_last[sf::Mouse::Right])))
    {
      //DEB(10,
          if(player().grenades > 0)
          {
            create_grenade(player(), get_mouse_pos(), 30.0f*(current_throw_amount/settings.max_throw_duration));
            current_throw_amount = 0.0f;
            player().grenades--;
            play_sound("throw");
          }else
          {
            play_sound("error");
            bottom_message("NO MORE GRENADES", sf::Color::Yellow, 30, sf::Color::Red);
          }
      //);
    }

    DEBUG_ONLY(
      if(ikp(sf::Keyboard::Num1))
      {
        DEB(30,
            gDrawSpawners = 1;
            gDrawMouse = 1;
            gDrawTargets = 1;
            gDrawNodes = 1;
            gDrawLights = 1;
            gDrawShaders = 0;
        );
      }


      if(ikp(sf::Keyboard::Num2))
      {
        DEB(30,
            gDrawSpawners = 0;
            gDrawMouse = 0;
            gDrawTargets = 0;
            gDrawNodes = 0;
            gDrawLights = 0;
            gDrawShaders = 1;
        );
      }
    );

    if(ikp(sf::Keyboard::G) && is_in_editor())
    {
      DEB(5,
      ed_mode += 1;
      ed_mode %= 3;
      );
    }

    if(ikp(sf::Keyboard::S) && key_modifiers[CTRL] && is_in_editor())
    {
      DEB(5,
        selected = nullptr;
        save_map(SSTR("assets/maps/" << level_names[selected_level]));
        bottom_message("Saved map", sf::Color::White, 30);
      );
    }

    if(ikp(sf::Keyboard::E))
    {
        if(status == RUNNING)
        {
         DEB(30,
            const float max_dist = 35.0f;
            const int max_angle = 50;
            if(get_mouse_pos()-player().position == v2f(0, 0))
              return;
            v2f direction = thor::unitVector(get_mouse_pos()-player().position);
            bool handled = false;
            for(auto item : items)
            {
              if(item->position == player().position)
                continue;
              v2f mob_direction = thor::unitVector(item->position-player().position);
              float angle = deg(std::acos(thor::dotProduct(direction, mob_direction)));
              float distance = magnitude(item->position-player().position);
              if(angle < max_angle && distance < max_dist)
              {
                player().inventory.push_back(item);
                items.erase(std::remove(items.begin(), items.end(), item));
                handled = true;
                break;
              }
            }
            if(!handled)
            {
              v2f direction = thor::unitVector(get_mouse_pos()-player().position);
              std::vector<ZPolygon*> close;
              for(auto& polygon : obstacles)
              {
                for(auto& line : polygon.lines)
                {
                  v2f p = closest_point_on_line(player().position, line);
                  float distance = magnitude(player().position - p);
                  v2f line_direction = thor::unitVector(p-player().position);
                  float angle = deg(std::acos(thor::dotProduct(direction, line_direction)));

                  //TODO activate closest
                  if(angle < max_angle && distance < max_dist)
                  {
                    close.push_back(&polygon);
                    break;
                  }
                }
              }

              // Find closest
              std::sort(close.begin(), close.end(), [=](ZPolygon* lhs, ZPolygon* rhs){
                int min_left = 9998, min_right = 9999;
                for(auto line : lhs->lines)
                {
                  int current = magnitude(player().position - closest_point_on_line(player().position, line));
                  if(current < min_left)
                    min_left = current;
                }

                for(auto line : rhs->lines)
                {
                  int current = magnitude(player().position - closest_point_on_line(player().position, line));
                  if(current < min_right)
					min_right = current;
                }

                return min_left < min_right;
              });


              for(ZPolygon* current : close)
              {
                float dontcare;
                for(auto line : current->lines)
                {
                  if(raycast(player().position, dontcare, line.from, POLYGON_GROUP_COMBINE_COLLIDE, current) || raycast(player().position, dontcare, line.to, POLYGON_GROUP_COMBINE_COLLIDE, current))
                  {
                    handled = true;
                    if(current->health <= 0)
                    {
                      if(score >= settings.repair_cost)
                      {
                        score -= settings.repair_cost;
                        current->health = current->max_health;
                        play_sound("nail", current->get_centroid());
                      }else
                      {
                        play_sound("error");
                        bottom_message("NOT ENOUGH SCORE TO REPAIR", sf::Color::Red, 30, sf::Color::Yellow);
                      }
                    }
                    else
                    {
                      if(current->group & POLYGON_GROUP_DOOR_CLOSED)
                      {
                        current->current_color = sf::Color(0, 0, 0, 0);
                        current->group = POLYGON_GROUP_DOOR_OPEN;
                        play_sound("door_open", current->get_centroid());
                      }
                      else if(current->group & POLYGON_GROUP_DOOR_OPEN)
                      {
                        current->current_color = sf::Color(0, 0, 0, 255);
                        current->group = POLYGON_GROUP_DOOR_CLOSED;
                        play_sound("door", current->get_centroid());
                      }
                      else if(current->group & POLYGON_GROUP_SHOP)
                      {
                        if(time.getElapsedTime().asSeconds() - shop_debounce > SHOP_DEB)
                        {
                          shop_debounce = time.getElapsedTime().asSeconds();
                          if(logic_status == LOGIC_SHOP_STAGE)
                          {
                            show_shop = true;
                          }else
                            play_sound("error");
                        }
                      }
                    }

                    break;
                  }

                }
              }
            }
          );
        }else if(status == PAUSED)
        {
          DEB(5,
            switch(ed_mode)
            {
              case EDITOR_MODE_POLYGON:
              {
                if(!selected)
                {
                  v2f p = ed_points.size() > 0 ? ed_clamped_mouse : get_mouse_pos();
                  if(!ed_float_grid)
                    p = v2f((int)p.x, (int)p.y);
                  add_action(std::make_shared<editor_action_point_add>(p));
                }
              }
              break;
              case EDITOR_MODE_ENTITY:
              {
                switch(ed_submode)
                {
                  case EDITOR_ENTITY_SUBMODE_SPAWNER:
                    create_spawner(get_mouse_pos());
                    break;
                  case EDITOR_ENTITY_SUBMODE_NAVNODE:
                    create_navnode(get_mouse_pos());
                    break;
                  case EDITOR_ENTITY_SUBMODE_LIGHT:
                  {
                    v2f light = get_mouse_pos();
                    static_lights.push_back(v3f_c(light));
                    break;
                  }
                  default:
                    break;
                }
              }
              break;
                  case EDITOR_MODE_ITEM:
                  {
                    add_action(std::make_shared<editor_action_create>(ghost_poly));
                  }
                  break;
                  default:
                    break;
            }
          );
        }
    }

    if(ikp(sf::Keyboard::T) && settings.show_editor)
    {
      DEB(5,
          player().position.x = get_mouse_pos().x;
          player().position.y = get_mouse_pos().y;
          player().velocity.x = player().velocity.y = 0;
      )
    }
  }
}

void Game::draw_vector(sf::Vector2f from, sf::Vector2f to)
{
  draw_vector(Line(from.x, from.y, to.x, to.y));
}

void Game::draw_vector(Line line)
{
  draw_line(line.from.x, line.from.y, line.to.x, line.to.y, sf::Color::Cyan);
  draw_circle(line.to.x, line.to.y, 6, sf::Color::Cyan);
}

void Game::draw_circle(float x, float y, float r, sf::Color color, bool fill)
{
  sf::CircleShape shape(r);

  shape.setFillColor(fill ? color : sf::Color::Transparent);
  shape.setOutlineColor(color);
  shape.setOutlineThickness(1.0f);
  shape.setOrigin(r, r);
  shape.setPosition(x, y);

  renderbuffer.draw(shape);
}


void Game::draw_line(float ax, float ay, float bx, float by, sf::Color tint,float size){
  float deltax = (bx - ax), deltay = (by - ay);
  float angle = Math::get_angle_between_coordinates(deltax, deltay);

  sf::RectangleShape rect(sf::Vector2f(size, float(sqrt(pow(deltax, 2)+pow(deltay, 2)))));
  rect.setRotation(angle);
  rect.setPosition(sf::Vector2f(bx, by));
  rect.setFillColor(tint);
  renderbuffer.draw(rect);
}

void Game::draw_sprite(float x1, float y1, float x2, float y2, std::string decal, float rotation, v2f scale, sf::Color tint, bool apply_shader)
{
  auto sprite = sprites[decal];
  if(decal.empty())
    return;
  v2f c = {(x1+x2)/2, (y1+y2)/2};
  int m = magnitude(camera_pos-c);
  if(m > 300)
    return;
  //sprite.setOrigin((x2-x1)/2, (y2-y1)/2);
  sprite.setOrigin(sprite.getTexture()->getSize().x/2, sprite.getTexture()->getSize().y/2);
  sprite.setScale(scale.x, scale.y);
  sprite.setPosition(x1, y1);
  sprite.setColor(tint);
  sprite.setRotation(rotation);
  if(apply_shader)
    renderbuffer.draw(sprite, &shadow_shader);
  else
    renderbuffer.draw(sprite);
}

sf::Vector2f Game::get_mouse_pos()
{
  sf::Vector2i rawpos = sf::Mouse::getPosition(sfWindow);
  v2f_t position = v2f_t(camera_pos) + (v2f_t(rawpos) - v2f_t(0.5f)*v2f_t(sfWindow.getSize().x, sfWindow.getSize().y))*v2f_t(get_zoom());

  if(is_in_editor())
  {
    float r = 4.0f;
    sf::CircleShape shape(r);
    shape.setOutlineColor(sf::Color::Red);
    shape.setOutlineThickness(2.0f);
    shape.setOrigin(r, r);
    shape.setFillColor(sf::Color::Transparent);
    shape.setPosition(position);
    renderbuffer.draw(shape);
  }

  return position;
}

void Game::clear()
{
  renderbuffer.clear(sf::Color::Black);
}

void Game::draw_editor()
{

#if NUKLEAR_ENABLED

  if (nk_begin(ctx, "Stats", nk_rect(sfWindow.getSize().x-300, 0, 250, 300),
    NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
    NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
  {
    nk_layout_row_dynamic(ctx, 20, 1);


    if(mStateMap.empty())
    {
      #define LOGIC_STATE(key) mStateMap[LOGIC_##key] = #key;
      LOGIC_STATES
      #undef LOGIC_STATE
    }

    nk_label(ctx, CSTR("Logic state: " << mStateMap[logic_status]), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Initial menu shown: " << (initial_menu_shown ? "true" : "false")) , NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Tick: " << tick), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Vtick: " << visual_tick), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Zoom: " << get_zoom()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Mobs: " << entities.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Obstacles: " << obstacles.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Sounds: " << play_queue.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Decals: " << decals.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Spawners: " << spawners.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Lights: " << static_lights.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Particles: " << particles.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Nodes: " << navnodes.size()), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("FPS: " << std::setprecision(4) << stats.last_fps), NK_TEXT_LEFT);
    nk_label(ctx, CSTR("Player: " << player().position.x << " " << player().position.y), NK_TEXT_LEFT);

    if(nk_button_label(ctx, "Free money"))
    {
      score += 20;
    }



    if(nk_button_label(ctx, "Clear map"))
    {
      obstacles.clear();
      selected = nullptr;
      hovered = nullptr;
    }

    if(nk_button_label(ctx, "Clear temps"))
    {
      decals.clear();
      particles.clear();
    }

    for(auto &boolPair : gGame.configs)
    {
      nk_checkbox_label(ctx, boolPair.first.c_str(), &boolPair.second);
    }

    for(auto action : editor_actions)
      nk_label(ctx, CSTR(action->get_label()), NK_TEXT_LEFT);

  }
  nk_end(ctx);

//   if (nk_begin(ctx, "Polygons", nk_rect(sfWindow.getSize().x-600, 0, 250, 300),
//     NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
//     NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
//   {
//     for(auto& current : obstacles)
//     {
//       nk_label(ctx, CSTR("FPS: " << std::setprecision(4) << stats.last_fps), NK_TEXT_LEFT);
//     }
//   }
//   nk_end(ctx);

  if (nk_begin(ctx, "Polygon properties", nk_rect(0, 400, 300, 300),
    NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
    NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
  {
    nk_layout_row_dynamic(ctx, 20, 1);
    if(selected)
    {
      if(nk_button_label(ctx, "Delete polygon"))
      {
        for(auto it = obstacles.begin(); it != obstacles.end(); it++)
        {
          if(&*it == selected)
          {
            selected = nullptr;
            hovered = nullptr;
            obstacles.erase(it);
            break;
          }
        }
      }

      int i;
      for(i = 0; i < (int)obstacles.size(); i++)
      {
        if(&obstacles[i] == selected)
          break;
      }

      int i_temp = i;
      nk_property_int(ctx, "Polygon position", 1, &i_temp, MAX_EDITOR_MOVE_FACTOR, 1, obstacles.size());

      if(i_temp != i)
      {
        DEB(1,
          std::rotate(obstacles.begin()+i_temp, obstacles.begin()+i, obstacles.begin()+i+1);
          selected = &*(obstacles.begin()+i_temp);
        );
      }

      nk_label(ctx, CSTR("Health: " << selected->health << "/" << selected->max_health), NK_TEXT_LEFT);
      nk_label(ctx, CSTR("Polygon groups: " << selected->group), NK_TEXT_LEFT);

      for(auto keyPair : mPolygonMap)
      {
        int current_flag = selected->group & keyPair.first;
        int previous = current_flag;
        nk_checkbox_label(ctx, keyPair.second.c_str(), &current_flag);
        if(previous != current_flag)
        {
          selected->group ^= keyPair.first;
        }
      }

      nk_label(ctx, CSTR("Polygon points: "), NK_TEXT_LEFT);
      for(auto point : selected->get_points())
      {
        nk_label(ctx, CSTR(point.x << ", " << point.y), NK_TEXT_LEFT);
      }
    }
  }
  nk_end(ctx);


  if (nk_begin(ctx, "Editor controls", nk_rect(0, 0, 250, 300),
    NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
    NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
  {
    nk_layout_row_dynamic(ctx, 20, 1);

    nk_property_float(ctx, "Move amount", 0.1f, &ed_move_factor, MAX_EDITOR_MOVE_FACTOR, 0.1f, 0.1f);
    nk_property_float(ctx, "Rotation amount", 0.1f, &ed_rotation_factor, MAX_EDITOR_MOVE_FACTOR, 0.5f, 0.5f);

    nk_label(ctx, CSTR("Polygon type"), NK_TEXT_LEFT);

    const char* poly_char[MAX_POLYGON];
    uint i = 0;
    for(auto keyPair : mPolygonMap)
    {
      char* temp = new char[100];
      strcpy(temp, keyPair.second.c_str());
      poly_char[i++] = temp;
    }
    nk_combobox(ctx, poly_char, mPolygonMap.size(), &ed_poly_group, 20, nk_vec2(100, 100));

    for(i = 0; i < mPolygonMap.size(); i++)
      delete poly_char[i];

    nk_label(ctx, CSTR("Spawn/Interact type"), NK_TEXT_LEFT);
    nk_combobox_separator(ctx, "POLYGON|ENTITY|ITEM", '|', &ed_mode, 3, 20, nk_vec2(100, 100));
    switch(ed_mode)
    {
      case EDITOR_MODE_ENTITY:
        nk_label(ctx, CSTR("Entity to spawn"), NK_TEXT_LEFT);
        nk_combobox_separator(ctx, "Spawner|NavNode|Light", '|', &ed_submode, 3, 20, nk_vec2(100, 100));
        break;
      case EDITOR_MODE_ITEM:
        {
          nk_label(ctx, CSTR("Item to spawn"), NK_TEXT_LEFT);

          const char* keys_char[MAX_POLYGON];
          uint i = 0;
          for(auto keyPair : predefined_polys)
          {
            char* temp = new char[100];
            strcpy(temp, keyPair.first.c_str());
            keys_char[i++] = temp;
          }

          static int choice;
          nk_combobox(ctx, keys_char, predefined_polys.size(), &choice, 20, nk_vec2(100, 100));
          ed_submode = choice;
          ghost_poly = predefined_polys[keys_char[choice]];
          ghost_poly.group |= POLYGON_GROUP_PREDEFINED;
          for(i = 0; i < predefined_polys.size(); i++)
            delete keys_char[i];

          //nk_combobox_begin(ctx, keys_char, predefined_polys.size(), &choice, nk_vec2(100, 100));
        }
        break;
      case EDITOR_MODE_POLYGON:
        {
//           nk_label(ctx, CSTR("ZPolygon texture:"), NK_TEXT_LEFT);
//           const char* textures[mTextures.size()-1];
//           for(uint i = 0; i < mTextures.size(); i++)
//           {
//             textures[i] = mTextures[i].c_str();
//           }
//           std::sort(mTextures.begin(), mTextures.end());
//
//           static int temp = std::find(mTextures.begin(), mTextures.end(), ed_poly_texture)-mTextures.begin();
//           nk_combobox(ctx, textures, mTextures.size(), &temp, 20, nk_vec2(100, 100));
//           ed_poly_texture = textures[temp];
        }
        break;
      default:
        break;
    }

    nk_checkbox_label(ctx, "Float grid", &ed_float_grid);

    static const int LEN = 255;
    static char buffer_map[LEN], buffer_des[LEN];
    static int len_map, len_des;

    if(ed_dirty_state)
    {
      memset(buffer_map, 0, LEN);
      memset(buffer_des, 0, LEN);
      strcpy(buffer_map, level_names[selected_level].c_str());
      strcpy(buffer_des, settings.map_description.c_str());
      len_map = level_names[selected_level].size();
      len_des =  settings.map_description.size();
      ed_dirty_state = false;
    }

    nk_label(ctx, "Map name:", NK_TEXT_LEFT);
    nk_edit_string(ctx, NK_EDIT_FIELD, buffer_map, &len_map, LEN, nk_filter_default);

    nk_label(ctx, "Map description:", NK_TEXT_LEFT);
    nk_edit_string(ctx, NK_EDIT_FIELD, buffer_des, &len_des, LEN, nk_filter_default);
    if(nk_button_label(ctx, "Save map"))
    {
      buffer_map[len_map] = 0;
      buffer_des[len_des] = 0;
      level_names[selected_level] = std::string(buffer_map);
      settings.map_description = std::string(buffer_des);
      save_map(SSTR("assets/maps/" << level_names[selected_level]));
      bottom_message("Saved map", sf::Color::White, 30);
    }

    if(nk_button_label(ctx, "Load map"))
    {
      reset_game(SSTR("assets/maps/" << level_names[selected_level]));
    }
    if(nk_button_label(ctx, "Save player pos to map"))
    {
      settings.player_position = player().position;
    }
  }


  if(ed_mode == EDITOR_MODE_ITEM)
  {
    v2f top_left = {-1, -1};
    for(auto& line : ghost_poly.lines)
    {
      if(top_left == v2f{-1, -1})
      {
        top_left = line.from + get_mouse_pos();
      }

      line.from = top_left - line.from;
      line.to = top_left - line.to;
    }
  }

  nk_end(ctx);
#endif
}

bool Game::raycast(sf::Vector2f start, ZPolygon& touch, sf::Vector2f target, int collide_groups, ZPolygon* ignore)
{
#if 1
  for(uint ii = 0; ii < obstacles.size(); ii++)
  {
    Line cast(start, target);
    auto& o = obstacles[ii];
    if(&o == ignore)
      continue;
    if((o.group & collide_groups) != 0)
    {
      for(auto line : obstacles[ii].lines)
      {
        if(line_collision(cast, line))
        {
          touch = obstacles[ii];
          return false;
        }
      }
    }
  }

  return true;
#endif
}

bool Game::raycast(sf::Vector2f start, float& length, sf::Vector2f target, int collide_groups, ZPolygon* ignore)
{
  sf::Vector2f temp;
  bool result = raycast(start, temp, target, collide_groups, ignore);
  length = magnitude(temp);
  return result;
}

bool Game::raycast(sf::Vector2f start, v2f& touch, sf::Vector2f target, int collide_groups, ZPolygon* ignore)
{
#if 1
  for(uint ii = 0; ii < obstacles.size(); ii++)
  {
    Line cast(start, target);
    auto& o = obstacles[ii];
    if(&o == ignore)
      continue;
    if((o.group & collide_groups) != 0)
    {
      for(auto line : obstacles[ii].lines)
      {
        if(line_collision(cast, line))
        {
          touch = closest_point_on_line(start, line);
          return false;
        }
      }
    }
  }

  return true;
#endif
}

void Game::draw_polygon(ZPolygon& polygon, sf::Color over, float thick)
{
  for(auto& line : polygon.lines)
  {
    sf::Vector2f c = line.center();
    if(gLineToEdge) draw_line(player().position.x, player().position.y, c.x, c.y, sf::Color::Red);
    if(gEdge) draw_line(line.from.x, line.from.y, line.to.x, line.to.y, sf::Color::Green);
    auto color = polygon.current_color;
    if(polygon.group & POLYGON_GROUP_DOOR_CLOSED)
    {
      color = sf::Color(1.0f-(polygon.health/(float)polygon.max_health)*255.0f, color.g, color.b, color.a);
    }
    if(over != sf::Color(0, 0, 0))
      color = over;
    if((polygon.group != 0 && !(polygon.group & POLYGON_GROUP_FLOOR)) || &polygon == selected || &polygon == hovered)
    {
      if(!(polygon.group & POLYGON_GROUP_PLAYER_ONLY) || status == PAUSED)
        draw_line(line.from.x, line.from.y, line.to.x, line.to.y, color, thick);
    }
  }
}

void Game::add_action(sp<editor_action> to_add)
{
  to_add->editor_do();
  editor_actions.push_back(to_add);
}

void Game::set_zoom(float absolute)
{
  float current = get_zoom();
  if(current < absolute)
  {
    while((current = get_zoom()) < absolute)
      zoom(1.04f, true);
  }
  else
  {
    while((current = get_zoom()) > absolute)
      zoom(0.96f, true);
  }

}

void Game::zoom(float modifier, bool force)
{
  bool can_zoom = false;
  if(status != PAUSED)
  {
    if(get_zoom() < 0.3 && modifier > 1.0f)
      can_zoom = true;
    if(get_zoom() > 0.08 && modifier < 1.0f)
      can_zoom = true;
  }
  else
    can_zoom = true;
  if(can_zoom || force)
  {
    zoom_current = modifier;
    view.zoom(zoom_current);
  }
}

float Game::get_zoom()
{
  return view.getSize().x/sfWindow.getSize().x;
}

bool Game::is_in_map_selection()
{
  if(menu_status == MENU_MAIN && logic_status == LOGIC_MENU)
    if(menu_selection == 1)
      return true;
  return false;
}

#define CREATE_FUNC(class) [=]{ return sp<class>(new class()); }
#define ITEM(name, price, class) shop_items.push_back(shop_item(price, price, name, CREATE_FUNC(class)));
void Game::draw_osd(float dt)
{
  const int sampler = 10;
  static sf::Clock last;
  if(stats.frames_since_last_reset > sampler)
  {
    stats.frames_since_last_reset = 0;
    stats.last_fps = sampler/stats.fps_counter.restart().asSeconds();;
    while(stats.fps_counter_data.size() > stats_struct::MAX_FPS_DATA_COUNT-1)
      stats.fps_counter_data.erase(stats.fps_counter_data.begin());

  }
  stats.fps_counter_data.push_back(1.0f/last.restart().asSeconds());
  stats.frames_since_last_reset++;
  //sfWindow.setTitle(SSTR(Game::TITLE << " - " << std::setprecision(4) << stats.last_fps));

  if(!message.done)
  {
    sf::Text text(message.text, sfml.font);
    text.setCharacterSize(message.font_size);
    text.setStyle(sf::Text::Bold);

    float transparency = 255.0f;
    float x = ((message.max_lifetime-message.lifetime)/(float)message.max_lifetime)*255.0f;
    //https://www.desmos.com/calculator
    transparency = 255.0f-std::tan(x/160)/0.003;
    if(transparency < 0){
      transparency = 0;
      message.done = true;
    }

    text.setFillColor(sf::Color(message.color.r, message.color.g, message.color.b, transparency));

    DEB(1, message.lifetime--;);

    //     renderbuffer.setView(view);
    switch(message.type)
    {
      case ONSCREEN:
        text.setPosition(sfWindow.getSize().x/2-text.getLocalBounds().width/2, sfWindow.getSize().y/2);
        sfWindow.setView(sfui);
        break;
      case ONMAP:
        text.setPosition(message.position);
        sfWindow.setView(view);
        break;
    }


    sfWindow.draw(text);
  }

  auto DoText = [=](int x, int y, int size, std::string textString, sf::Color col = sf::Color::White, bool offset = false)
  {
    sf::Text text(textString, sfml.font);
    text.setCharacterSize(size);
    text.setStyle(sf::Text::Bold);
    text.setFillColor(col);
    text.setPosition(x - (offset ? text.getLocalBounds().width/2 : 0), y);
    sfWindow.draw(text);
  };

  if(status == PAUSED)
  {
    if(is_in_editor())
      draw_editor();
    else
    {
      if(logic_status != LOGIC_MENU)
      {
        static int max = 60;
        if(effects.pause_menu_flash % max == 0)
          center_message("PAUSED - PRESS P TO UNPAUSE", sf::Color::Yellow, 40);
        effects.pause_menu_flash++;
        effects.pause_menu_flash %= max;
      }
    }
  }

  if(show_shop)
  {
    menu_size = shop_items.size();

    if(shop_items.empty())
    {
      ITEM("SHOTGUN", 25, Shotgun);
      ITEM("UZI", 15, Uzi);
      ITEM("UZI AKIMBO", 30, UziAkimbo);
      ITEM("MEDKIT", 85, Medkit);
      ITEM("GRENADE", 20, Medkit);
    }

    int i = -1, size = 40;
    DoText(sfWindow.getSize().x/2, sfWindow.getSize().y/2+i++*size, size, "ITEM SHOP", sf::Color::White, true);
    for(auto it = shop_items.begin(); it != shop_items.end(); it++)
    {
      auto &item = *it;
      std::string display = SSTR(item.tag << " : " << item.price);
      sf::Color textColor = sf::Color::Yellow;
      if(i == menu_selection)
      {
        display = SSTR(">" << display << "<");
        if(effects.bought_frames % 2 == 1) textColor = sf::Color::Red;
      }

      DoText(sfWindow.getSize().x/2, sfWindow.getSize().y/2+i++*size, size, display, textColor, true);
    }

    std::string display;
    if((int)shop_items.size() == menu_selection)
      display = ">EXIT SHOP<";
    else
      display = "EXIT SHOP";
    DoText(sfWindow.getSize().x/2, sfWindow.getSize().y/2+i++*size, size, display, sf::Color::Yellow, true);
  }
  else if(logic_status == LOGIC_MENU)
  {
    auto ChangeMenuFn = [=](int newstatus){ menu_status = (MENU_STATUS)newstatus; menu_items.clear(); menu_selection = 0; };
    static auto nFn = []{ };
    if(menu_items.empty())
    {
      switch(menu_status)
      {
        case MENU_MAIN:
        {
          menu_items.push_back(menu_item((!initial_menu_shown || changed_map) ? "START" : "RESUME",
              [=]{
                if(initial_menu_shown)
                {
                  logic_status = previous_logic_status;
                  status = RUNNING;
                }
                else
                {
                  set_logic(LOGIC_ONGOING);
                  initial_menu_shown = true;
                };
                if(previous_zoom > 0)
                {
                  set_zoom(previous_zoom);
                  previous_zoom = -1.0f;
                }
                ed_dirty_state = true;
                changed_map = false;
                menu_items.clear();
          }));
          menu_items.push_back(menu_item(
            [=]{
              std::string temp = level_names[selected_level];
              std::transform(temp.begin(), temp.end(), temp.begin(), ::toupper);
              return  SSTR("MAP: " << temp);
            },
            [=]{ reset_game(SSTR("assets/maps/" << level_names[selected_level])); changed_map = true; menu_items.clear(); },
            [=]{
              initial_zooming_completed = true;
              if(previous_zoom < 0)
                previous_zoom = get_zoom();
              set_zoom(1.0f);
              --selected_level; selected_level = (selected_level < 0 ? level_names.size()-1 : selected_level);
              bottom_message("PRESS E TO CHANGE MAP", sf::Color::Yellow, 30, sf::Color::Red, 300);
            },
            [=]{
              initial_zooming_completed = true;
              if(previous_zoom < 0)
                previous_zoom = get_zoom();
              set_zoom(1.0f);
              ++selected_level; selected_level = (selected_level == (int)level_names.size() ? 0 : selected_level);
              bottom_message("PRESS E TO CHANGE MAP", sf::Color::Yellow, 30, sf::Color::Red, 300);
            },
            5,
            5
          ));
          menu_items.push_back(menu_item("SETTINGS", [=]{ ChangeMenuFn(MENU_SETTING); }));
		  menu_items.push_back(menu_item("VIDEO OPTIONS", [=] { ChangeMenuFn(MENU_VIDEO); }));
          menu_items.push_back(menu_item("QUIT", [=]{ status = QUITTING; }));
          break;
        }
        case MENU_SETTING:
        {
          menu_items.push_back(menu_item(
            [=]{ return SSTR("VOLUME: " << settings.volume); },
            nFn,
            [=]{ settings.volume -= 1; settings.volume = settings.volume < 0 ? 0 : settings.volume; },
            [=]{ settings.volume += 1; settings.volume = settings.volume > 100 ? 100 : settings.volume; }
          ));
          menu_items.push_back(menu_item("BACK", [=]{ save_settings(); ChangeMenuFn(MENU_MAIN); }));
          break;
        }
		case MENU_VIDEO:
		{
			menu_items.push_back(menu_item(
				[=] { return SSTR("VSYNC: " << (settings.v_sync ? "ON" : "OFF")); },
				[=] { settings.v_sync = !settings.v_sync; },
				[=] { settings.v_sync = !settings.v_sync; },
				[=] { settings.v_sync = !settings.v_sync; },
				10,
				10
			));
			menu_items.push_back(menu_item(
				[=] { return SSTR("FULLSCREEN: " << (settings.sf_window_style == sf::Style::Fullscreen ? "ON" : "OFF")); },
				[=] { settings.sf_window_style == sf::Style::Fullscreen ? settings.sf_window_style = sf::Style::Default : settings.sf_window_style = sf::Style::Fullscreen; },
				[=] { settings.sf_window_style == sf::Style::Fullscreen ? settings.sf_window_style = sf::Style::Default : settings.sf_window_style = sf::Style::Fullscreen; },
				[=] { settings.sf_window_style == sf::Style::Fullscreen ? settings.sf_window_style = sf::Style::Default : settings.sf_window_style = sf::Style::Fullscreen; },
				10,
				10
			));

			menu_items.push_back(menu_item(
				[=] { return SSTR("BORDERLESS: " << (settings.sf_window_style == sf::Style::None ? "ON" : "OFF")); },
				[=] { settings.sf_window_style == sf::Style::None ? settings.sf_window_style = sf::Style::Default : settings.sf_window_style = sf::Style::None; },
				[=] { settings.sf_window_style == sf::Style::None ? settings.sf_window_style = sf::Style::Default : settings.sf_window_style = sf::Style::None; },
				[=] { settings.sf_window_style == sf::Style::None ? settings.sf_window_style = sf::Style::Default : settings.sf_window_style = sf::Style::None; },
				10,
				10
			));
			
			menu_items.push_back(menu_item(
				[=] {
					sf::VideoMode c = settings.video_modes.at(settings.hovered_mode_index);
					return SSTR(c.width << "x" << c.height << "x" << c.bitsPerPixel << (settings.current_mode.width == c.width && settings.current_mode.height == c.height ? " (SELECTED)" : "")); },
				[=] { ; settings.selected_mode_index = settings.hovered_mode_index; },
				[=] { settings.hovered_mode_index -= 1; if (settings.hovered_mode_index < 0) settings.hovered_mode_index = settings.video_modes.size()-1; },
				[=] { settings.hovered_mode_index += 1; if (settings.hovered_mode_index > settings.video_modes.size()-1) settings.hovered_mode_index = 0; },
				8,
				8
			));

			menu_items.push_back(menu_item("BACK", [=] { save_settings(); ChangeMenuFn(MENU_MAIN); }));
			break;
		}
         case MENU_QUIT:
        {
          //TODO change menu fn
          menu_selection = 1;
          static bool really_quit = false;
          menu_items.push_back(menu_item("REALLY QUIT?", nFn));
          menu_items.push_back(menu_item(
            [=]{ return SSTR((really_quit ? "YES" : "NO")); },
            [=]{ if(really_quit){ status = QUITTING; }else{ menu_status = MENU_MAIN; menu_items.clear(); } },
            [=]{ really_quit = !really_quit; },
            [=]{ really_quit = !really_quit; },
            15,
            15
          ));
          break;
        }
      };
    }
    menu_size = menu_items.size()-1;
    int i = -1, size = 40;
    DoText(sfWindow.getSize().x/2, sfWindow.getSize().y/2+i++*size, size, "MAIN MENU", sf::Color::White, true);
    for(auto current : menu_items)
    {
      sf::Color selected = (i == menu_selection ? sf::Color::Red : sf::Color::Yellow);
      std::string label = current.label;
      if(label.empty())
        label = current.text_update();
      if(current.has_left_right_options && i == menu_selection)
        label = SSTR("< " << label << " >");
      DoText(sfWindow.getSize().x/2, sfWindow.getSize().y/2+i++*size, size, label, selected, true);
    }

    if(is_in_map_selection())
      DoText(sfWindow.getSize().x/2, 0, 20, SSTR("MAP DESCRIPTION: " << settings.map_description), sf::Color::Yellow, true);
  }

  if(player().item)
  {
    auto gun = std::dynamic_pointer_cast<Gun>(player().item);
    int i = 3, s = 30, l = 2;

    if(gun && gun->ammo != -1)
    {
      for(int j = 1; j < l; j++)
        DoText(sfWindow.getSize().x-140+j, sfWindow.getSize().y-12-(s*i)+j*2, s, CSTR("RNDS " << gun->ammo), sf::Color(255, 255, 255, 255/j));
    }
    i--;

    {
      for(int j = 1; j < l; j++)
        DoText(sfWindow.getSize().x-140+j, sfWindow.getSize().y-12-(s*i)+j*2, s, CSTR("GREN " << player().grenades), sf::Color(255, 255, 255, 255/j));
      i--;
    }

    {
      for(int j = 1; j < l; j++)
        DoText(sfWindow.getSize().x-140+j, sfWindow.getSize().y-12-(s*i)+j*2, s, CSTR("SCORE " << score), sf::Color(255, 255, 255, 255/j));
      i--;
    }


    {
      std::string text;
      sf::Color col = sf::Color::White;
      if(logic_status == LOGIC_SHOP_STAGE)
      {
        text = CSTR("TIM LEFT: " << std::setprecision(2) << rules.next_round_in);
      }
      else
      {
        text = CSTR("ZOM LEFT: " << zombies_left);
        if(zombies_left < 5)
          col = sf::Color::Yellow;
      }

      for(int j = 1; j < l; j++)
        DoText(0+j, sfWindow.getSize().y-40+j, 30, text, col);
    }

    static int flash = 0, flash_length = 120;
    DEB(1,
    flash += 1;
    );
    flash %= flash_length;
    for(int j = 1; j < l; j++)
      DoText(sfWindow.getSize().x/2, sfWindow.getSize().y-40, bottom_message_t.font_size, bottom_message_t.text, flash > flash_length/2 ? bottom_message_t.color : bottom_message_t.flash_color, true);
  }

  auto IsInFn = [=](v2f mouse, v2f other, float i_size)
  {
    return mouse.x > other.x && mouse.x < other.x + i_size &&
    mouse.y > other.y && mouse.y < other.y + i_size;
  };

  if(selected && is_in_editor())
  {
    sf::RectangleShape back;
    back.setSize({(float)sfWindow.getSize().x, 60});
    back.setPosition(0, sfWindow.getSize().y-60);
    back.setFillColor(sf::Color::Black);
    auto sprite = sprites[ed_poly_texture_hovered];
    sprite.setPosition(5, sfWindow.getSize().y-BOTTOM_PANEL_HEIGHT);
    auto size = sprite.getTexture()->getSize();
    sprite.setScale(1/(size.x/50.0f), 1/(size.y/50.0f));
    sfWindow.draw(back);
    back.setSize({50.0f, 50.0f});
    back.setFillColor(sf::Color::Transparent);
    back.setOutlineColor(sf::Color::White);
    back.setOutlineThickness(1.0f);
    back.setPosition(sprite.getPosition());
    sfWindow.draw(sprite);
    sfWindow.draw(back);
    DoText(70, sfWindow.getSize().y-55, 15, SSTR("Current texture : " << ed_poly_texture_hovered), sf::Color::White, false);

    const float i_size = 20.0f;
    const int width_offset = 600;
    const int per_row = std::floor(width_offset/i_size)-3;
    static int scroll_offset = 0;
    int max_displayed = scroll_offset+(per_row*2);
    max_displayed = max_displayed > (int)mTextures.size() ? mTextures.size() : max_displayed;
    const int height_offset = BOTTOM_PANEL_HEIGHT;
    v2f m_pos = v2f(sf::Mouse::getPosition(sfWindow));
    bool any_in = false;
    for(int i = scroll_offset; i < max_displayed; i++)
    {
      int real_i = i-scroll_offset;
      int row = real_i/per_row;
      int column = real_i%per_row;
      v2f pos = v2f(sfWindow.getSize().x-width_offset+(column*i_size+(column*2)), sfWindow.getSize().y-height_offset+(row*i_size+(row*2)));
      auto sprite = sprites[mTextures[i]];
      sprite.setPosition(pos);
      auto size = sprite.getTexture()->getSize();
      sprite.setScale(1/(size.x/i_size), 1/(size.y/i_size));
      sf::RectangleShape back;
      back.setSize({i_size, i_size});
      back.setFillColor(sf::Color::Transparent);
      back.setOutlineThickness(1.5f);
      back.setPosition(sprite.getPosition());
      bool is_in = IsInFn(m_pos, back.getPosition(), i_size);
      if(is_in)
      {
        ed_poly_texture_hovered = mTextures[i];
        any_in = true;
      }
      if(is_in && mouse_down[sf::Mouse::Left])
      {
        ed_poly_texture = mTextures[i];
        if(selected)
          selected->texture_label = ed_poly_texture;
      }
      back.setOutlineColor(is_in ? sf::Color::Red : sf::Color::White);
      if(mTextures[i] == ed_poly_texture)
        back.setOutlineColor(sf::Color::Yellow);
      sfWindow.draw(sprite);
      sfWindow.draw(back);
    }
    if(!any_in)
      ed_poly_texture_hovered = ed_poly_texture;

    auto arrow = sprites["arrow"];
    size = arrow.getTexture()->getSize();
    arrow.setPosition(sfWindow.getSize().x-width_offset-30, sfWindow.getSize().y-40);
    arrow.setOrigin(size.x/2.0f, size.y/2.0f);
    arrow.setScale(1/(size.x/20.0f), 1/(size.y/20.0f));
    if(IsInFn(m_pos, arrow.getPosition()-v2f(10.0f, 10.0f), 20.0f))
    {
      arrow.setColor(sf::Color::Red);
      if(mouse_down[sf::Mouse::Left] && scroll_offset > 0)
      {
        DEB(5, scroll_offset -= per_row; play_sound("select"););
        if(scroll_offset < 0)
          scroll_offset = 0;
      }
    }
    if(scroll_offset > 0)
    {
      sfWindow.draw(arrow);
    }
    arrow.setColor(sf::Color::White);
    arrow.setRotation(180.0f);
    arrow.setPosition(sfWindow.getSize().x-width_offset-30, sfWindow.getSize().y-15);
    if(IsInFn(m_pos, arrow.getPosition()-v2f(10.0f, 10.0f), 20.0f))
    {
      arrow.setColor(sf::Color::Red);
      if(mouse_down[sf::Mouse::Left] && scroll_offset < max_displayed-per_row)
      {
        DEB(5, scroll_offset += per_row; play_sound("select"););
        if(scroll_offset > (int)(mTextures.size()-per_row))
          scroll_offset = mTextures.size()-per_row;
      }
    }
    if(scroll_offset < max_displayed-per_row)
      sfWindow.draw(arrow);
  }
}

void Game::draw(float dt)
{
  static int last_mode = -1;
  if (last_mode != settings.selected_mode_index)
  {
	  settings.current_mode = settings.video_modes.at(settings.selected_mode_index);
	  sfWindow.create(sf::VideoMode(settings.current_mode.width, settings.current_mode.height), "ZuMbo MumBo", settings.sf_window_style);
	  view = sf::View(sf::FloatRect(0, 0, settings.current_mode.width, settings.current_mode.height));
	  sfui = sf::View(sf::FloatRect(0, 0, settings.current_mode.width, settings.current_mode.height));
	  sfWindow.setView(view);
	  renderbuffer.create(settings.current_mode.width, settings.current_mode.height);
	  view.setCenter(camera_pos.x, camera_pos.y);
	  if (initial_zooming_completed)
		  set_zoom(0.32f);
	  last_mode = settings.selected_mode_index;
  }
  static int last_fullscreen_mode = sf::Style::Default;
  if (last_fullscreen_mode != settings.sf_window_style)
  {
	  sfWindow.create(sf::VideoMode(settings.current_mode.width, settings.current_mode.height), "ZuMbo MumBo", settings.sf_window_style);
	  last_fullscreen_mode = settings.sf_window_style;
  }

  DEB(1, bottom_message_t.lifetime--;);
  if(bottom_message_t.lifetime <= 0)
  {
    bottom_message_t.text = "";
    bottom_message_t.lifetime = 0;
  }

  if(&renderbuffer.getDefaultView() != &view)
    renderbuffer.setView(view);


  if(mPolygonMap.empty())
  {
    #define POLYGON_GROUP_DEF(key, value) mPolygonMap[value] = #key;
    POLYGON_GROUPS
    #undef POLYGON_GROUP_DEF
  }

  if(settings.lock_camera_to_player && logic_status != LOGIC_LOST)
  {
    v2f desired_center = player().position;;
    if(effects.recoil_frames > 0)
    {
      v2f diff = Math::norm(get_mouse_pos()-player().position);
      desired_center += diff * 9.0f * (effects.recoil_frames/(float)effects.max_recoil_frames);
    }
    camera_pos = lerp(camera_pos, settings.camera_smooth*dt, desired_center);
  }

  view.setCenter(camera_pos.x, camera_pos.y);

  if(logic_status == LOGIC_WON)
  {
    for(int i = 0; i < 5; i++)
    {
      auto& obs = obstacles.at(rand()%obstacles.size());
      effects.lights.push_back(v3f_c(obs.get_centroid(), 2));
    }
  }

  for(auto& polygon : obstacles)
  {
    std::vector<sf::Vector2f> points;
    for(auto& line : polygon.lines)
    {
      points.push_back(line.from);
      points.push_back(line.to);
    }

    //std::sort( points.begin(), points.end(),  );
    points.erase( std::unique( points.begin(), points.end() ), points.end() );
    sf::ConvexShape shape;
    shape.setPointCount(points.size());
    for(uint i = 0; i < points.size(); i++)
    {
      shape.setPoint(i, points[i]);
    }

    v2f c = polygon.get_centroid();
    int m = magnitude(camera_pos-c);
      if(m > 350 && polygon.texture_label != "gravel")
        continue;

    auto& tex = textures[polygon.texture_label];
    tex.setRepeated(true);
    shape.setTexture(&tex);
    if(!(polygon.group & POLYGON_GROUP_NO_BORDER))
    {
      shape.setOutlineColor(sf::Color::Black);
      shape.setOutlineThickness(0.5f);
    }

    if(!(polygon.group & POLYGON_GROUP_PREDEFINED))
    {
      auto bound = shape.getLocalBounds();
      bound = {bound.left/settings.texture_scale, bound.top/settings.texture_scale, bound.width/settings.texture_scale, bound.height/settings.texture_scale};
      shape.setTextureRect({(int)bound.left, (int)bound.top, (int)bound.width, (int)bound.height});
    }

    if((polygon.group & POLYGON_GROUP_COMBINE_REPAIRABLE) == 0 || polygon.health > 0)
    {
      if((polygon.group & POLYGON_GROUP_PLAYER_ONLY) == 0)
      {
        float health_ratio = 100.0*(polygon.health/(float)polygon.max_health)+155.0f;
        shape.setFillColor(sf::Color(255, health_ratio, health_ratio, 255));


        if(polygon.group & POLYGON_GROUP_DOOR_OPEN)
          shape.setFillColor(sf::Color(255, 255, 255, 170));
        renderbuffer.draw(shape);
        if(polygon.max_health*0.75 > polygon.health)
        {
          sf::ConvexShape cracks = shape;
          cracks.setTexture(&textures["cracks"]);
          cracks.setFillColor(sf::Color(255, 255, 255, 255-health_ratio));
          renderbuffer.draw(cracks);
        }else if(polygon.group & POLYGON_GROUP_SHOP && logic_status == LOGIC_SHOP_STAGE)
        {
          sf::ConvexShape glow = shape;
          glow.setFillColor(sf::Color(255, 255, 0, 30+sin(time.getElapsedTime().asSeconds()*5.0f)*150.0f));
          renderbuffer.draw(glow);
        }

      }
    }

    if(is_in_editor())
    {
      v2f pos = polygon.get_centroid();
      draw_circle(pos.x, pos.y, 1.0f, sf::Color::Blue);
    }
  }

  if(is_in_editor())
  {
    sf::ConvexShape shape;
    shape.setPointCount(ed_points.size());
    for(uint i = 0; i < ed_points.size(); i++)
    {
      shape.setPoint(i, ed_points[i]);
      sf::CircleShape circle(2);
      circle.setFillColor(sf::Color::Magenta);
      circle.setOrigin(2, 2);
      circle.setPosition(ed_points[i].x, ed_points[i].y);
      renderbuffer.draw(circle);
    }
    shape.setOutlineColor(sf::Color::Magenta);
    shape.setOutlineThickness(2);
    renderbuffer.draw(shape);


    if(ed_points.size() > 0)
    {
      v2f new_to;
      v2f from = ed_points.back();
      if(ikp(sf::Keyboard::F))
      {
        v2f to = get_mouse_pos();

        v2f diff = to-from;
        if(std::abs(diff.x) > std::abs(diff.y))
          new_to = v2f(get_mouse_pos().x, from.y);
        else
          new_to = v2f(from.x, get_mouse_pos().y);
      }
      else
      {
        v2f to = get_mouse_pos();

        int angle = Math::get_angle_between_coordinates(to.x, to.y, from.x, from.y) - 90;
        if(to != from)
        {
          float dist = magnitude(from-to);

          int clamped_angle = angle / ed_angle_lock;
          clamped_angle *= ed_angle_lock;
          new_to = v2f(from.x + cos(rad(clamped_angle))*dist, from.y + sin(rad(clamped_angle))*dist);

        }
      }
      ed_clamped_mouse = new_to;
      if(!ed_float_grid)
        new_to = v2f((int)new_to.x, (int)new_to.y);
      draw_line(from.x, from.y, new_to.x, new_to.y, sf::Color::Red, 3);
    }
  }


  for(uint i = 0; i < decals.size(); i++)
  {
    Entity& e = decals[i];
    draw_sprite(e.position.x, e.position.y, e.position.x+e.w, e.position.y+e.h, e.decal, e.rotation, v2f(0.3f, 0.3f));
  }

  if(ed_mode == EDITOR_MODE_ENTITY)
  {
    auto IsIn = [=](v2f center, float r, v2f point){
      float radius_sum = r + 2.0f;
      v2f d = point - center;
      float l = thor::length(d);

      return (l < radius_sum);
    };

    v2f pos = get_mouse_pos();
    for(auto& n : navnodes)
    {
      if(gDrawNodes)
        draw_circle(n.position.x, n.position.y, NODE_RADIUS, sf::Color::Green);
      if(IsIn(n.position, NODE_RADIUS, pos))
      {
        draw_circle(n.position.x, n.position.y, NODE_RADIUS, sf::Color::Red, false);
        if(mouse_down[sf::Mouse::Right])
          navnodes.erase(std::remove_if(navnodes.begin(), navnodes.end(), [=](const auto& o){ return o.position == n.position; }), navnodes.end());
      }
    }

    for(auto& n : static_lights)
    {
      if(gDrawLights)
        draw_circle(n.x, n.y, NODE_RADIUS, sf::Color::White);
      if(IsIn(v2f(n.x, n.y), NODE_RADIUS, pos))
      {
        draw_circle(n.x, n.y, NODE_RADIUS, sf::Color::Red, false);
        if(mouse_down[sf::Mouse::Right])
          static_lights.erase(std::remove_if(static_lights.begin(), static_lights.end(), [=](const auto& o){ return v2f(o.x, o.y) == v2f(n.x, n.y); }), static_lights.end());
      }
    }

    for(auto& n : spawners)
    {
      if(gDrawSpawners)
        draw_circle(n.position.x, n.position.y, NODE_RADIUS, sf::Color::Cyan);
      if(IsIn(n.position, NODE_RADIUS, pos))
      {
        draw_circle(n.position.x, n.position.y, NODE_RADIUS, sf::Color::Red, false);
        if(mouse_down[sf::Mouse::Right])
          spawners.erase(std::remove_if(spawners.begin(), spawners.end(), [=](const auto& o){ return o.position == n.position; }), spawners.end());
      }
    }

  }

  for(auto& s : items)
  {
    sf::Color target = sf::Color::Yellow;
    switch(s->group)
    {
      case ENTITY_GROUP_MEDKIT:
        target = sf::Color::Red;
        break;
      default:
        break;
    }
    draw_circle(s->position.x, s->position.y, 5.0f, target);
  }

  draw_polygon(interact_poly, sf::Color(255, 69, 0, 128), 0.6f+sin(time.getElapsedTime().asSeconds()*5.0)*0.3f);

  for(auto& particle : particles)
  {
    v2f p = particle.position;
    switch(particle.type)
    {
      case PARTICLE_BLOOD:
        draw_sprite(p.x, p.y, p.x+particle.size, p.y+particle.size, "blood_part", particle.rotation, v2f(0.1f, 0.1f)*particle.size);
        break;
      case PARTICLE_SHOTGUN_CASING:
        draw_sprite(p.x, p.y, p.x+particle.size, p.y+particle.size, "shotguncasing", particle.rotation, v2f(1.0f, 1.0f)*particle.size);
        break;
      case PARTICLE_GUN_CASING:
        draw_sprite(p.x, p.y, p.x+particle.size, p.y+particle.size, "guncasing", particle.rotation, v2f(1.0f, 1.0f)*particle.size);
        break;
      case PARTICLE_BRAIN:
        draw_circle(particle.position.x, particle.position.y, particle.size, particle.color);
        break;
    }
  }

  for(uint i = 0; i < entities.size(); i++)
  {
    Entity& e = entities[i];
    if(!(e.group & ENTITY_GROUP_BULLET) && !(e.group & ENTITY_GROUP_GRENADE))
      e.color.r = (1.0f-e.health/(float)e.max_health)*255;

    if(&player() == &e)
      continue;


    v2f p = e.position;
    float ang = Math::get_angle_between_coordinates(p.x, p.y, e.target.x, e.target.y)-180;
    if(e.group & ENTITY_GROUP_BULLET)
    {
      switch(e.extra_group)
      {
        case PROJECTILE_BULLET:
          draw_sprite(e.position.x, e.position.y, e.position.x + e.w, e.position.y + e.h, "bullet", e.rotation-180, {e.w, e.w});
          break;
        case PROJECTILE_SHRAPNEL:
          draw_sprite(e.position.x, e.position.y, e.position.x + e.w, e.position.y + e.h, "shrapnel", e.rotation-180, {e.w, e.w});
          break;
      }
    }
    else if(e.group & ENTITY_GROUP_MOB)
    {
      std::string anim_string = "zombie_";
      switch(e.anim_state)
      {
        case ANIM_LEFT_ARM: anim_string += "left"; break;
        case ANIM_RIGHT_ARM: anim_string += "right"; break;
        case ANIM_BOTH_ARMS: anim_string += "idle"; break;
      }
      anim_string += SSTR("_" << e.base_sprite);
      draw_sprite(p.x, p.y, p.x + e.w, p.y + e.w, anim_string, ang, {1.0f, 1.0f});
    }
    else if(e.group & ENTITY_GROUP_GRENADE)
    {
      draw_sprite(e.position.x, e.position.y, e.position.x+e.w, e.position.y+e.h, "grenade", e.rotation, v2f(0.4, 0.4));
    }


    if(e.group & ENTITY_GROUP_MOB)
    {
      v2f facing;
      if(e.target != v2f(-1, -1))
      {
        facing = e.target;
        if(gDrawTargets)
          draw_line(e.position.x, e.position.y, e.target.x, e.target.y, sf::Color::Green);
      }

      draw_blood(e);
    }
    draw_circle(sfWindow.getSize().x/2.0f, 20, 20.0f, sf::Color::Cyan);
    draw_circle(sfWindow.getSize().x/2.0f, 20, 20.0f, sf::Color::Red);
    draw_circle((sfWindow.getSize().x+(sfWindow.getSize().x-sfWindow.getSize().x)/2.0f), 20, 20.0f, sf::Color::Blue);

  }


  if(gDrawMouse) draw_line(player().position.x, player().position.y, get_mouse_pos().x, get_mouse_pos().y);

#if NUKLEAR_ENABLED
  //DEBUG_ONLY(
    if(status == PAUSED)
    {
      if(ed_mode == EDITOR_MODE_ITEM)
        draw_polygon(ghost_poly);
      if(hovered && ed_mode == EDITOR_MODE_POLYGON)
        draw_polygon(*hovered, sf::Color::Cyan);
      if(selected && ed_mode == EDITOR_MODE_POLYGON)
        draw_polygon(*selected, sf::Color::Red);
    }
  //);
#endif

  v2f p = player().position;
  float ang = Math::get_angle_between_coordinates(p.x, p.y, get_mouse_pos().x, get_mouse_pos().y)-180;
  if(logic_status != LOGIC_LOST)
  {
    if(player().item)
    {
      draw_sprite(p.x, p.y, p.x + player().w, p.y + player().w, player().item->two_handed ? "player_two" : "player", ang, {1.0f, 1.0f});
    }

    player().target = get_mouse_pos();
    draw_blood(player());

    if(logic_status == LOGIC_WON)
    {
      draw_sprite(p.x, p.y, p.x + player().w, p.y + player().w, "hat", ang, {1.0f, 1.0f}, sf::Color(255, 255, 255, 255), false);
    }

    // Draw gun
    v2f d = get_mouse_pos()-player().position;
    if(thor::length(d) > EPSILON)
    {
      v2f mouse_direction = thor::unitVector(d);
      v2f normal_vector = thor::perpendicularVector(mouse_direction);
      v2f mouse_vector = mouse_direction*30.0f;
      v2f gun_base = player().position + normal_vector - (mouse_vector/2.0f);
      player().muzzle_location = gun_base+mouse_vector; // TODO somewhere else
      v2f good_gun_base = player().muzzle_location - thor::unitVector(mouse_vector)*12.0f;
      v2f dire = Math::norm(player().muzzle_location-get_mouse_pos())*10.0f;
      good_gun_base += dire*(effects.recoil_frames/(float)effects.max_recoil_frames) + (mouse_vector/2.0f);
      float angle = Math::get_angle_between_coordinates(good_gun_base.x, good_gun_base.y, good_gun_base.x+mouse_vector.x, good_gun_base.y+mouse_vector.y)-180;
      draw_sprite(good_gun_base.x, good_gun_base.y, good_gun_base.x + 100, good_gun_base.y + 100, player().item->visual_label, angle, {player().item->scale, player().item->scale});
    }
  }else
  {
    draw_sprite(p.x, p.y, p.x + player().w, p.y + player().w, SSTR("zombie_idle_man"), ang, {1.0f, 1.0f});
    draw_blood(player());
  }

  static int glow = 90;
  static bool going_right = true;
  if(glow > 240)
    going_right = false;
  else if(glow < 90)
    going_right = true;

  DEB(1,
    if(going_right)
      ++glow;
    else
      --glow;
  );

  DEB(1,
    if(effects.recoil_frames > 0)
      effects.recoil_frames--;
  );

  if(effects.flash_frames > 0)
  {
    const int max_flash = 5;
    sf::RectangleShape shadow(v2f(sfWindow.getSize().x, sfWindow.getSize().y));
    shadow.setFillColor(sf::Color(200, 200, 200, max_flash-(effects.flash_frames%max_flash)/(float)max_flash*255.0f));
    shadow.setPosition(-10, 115); // idk why
    renderbuffer.draw(shadow);
    DEB(1,
      effects.flash_frames--;
    );
  }

#if NUKLEAR_ENABLED
  float bg[4];
  nk_color_fv(bg, background);
#endif

  sprites["screen"].setTexture(renderbuffer.getTexture(), true);
  sf::Sprite temp(renderbuffer.getTexture());
  temp.setScale(1, -1);
  temp.setOrigin(0, sfWindow.getSize().y);
  shadow_shader.setUniform("texture", sf::Shader::CurrentTexture);
  shadow_shader.setUniform("pos", player().position);
  shadow_shader.setUniform("time", time.getElapsedTime().asSeconds());
  float hp_percent = player().health/(float)player().max_health;
  shadow_shader.setUniform("percent_health", hp_percent);
  shadow_shader.setUniform("percent_stamina", player().stamina/(float)player().max_stamina);
  shadow_shader.setUniform("percent_throw", current_throw_amount/(float)settings.max_throw_duration);
  shadow_shader.setUniform("resolution", v2f(sfWindow.getSize().x, sfWindow.getSize().y));
  shadow_shader.setUniform("cam_pos_ws", camera_pos);
  shadow_shader.setUniform("zoom", get_zoom());
  shadow_shader.setUniform("active_lights", std::min((int)effects.lights.size(), MAX_LIGHT));
  shadow_shader.setUniformArray("lights", &effects.lights[0], std::min((int)effects.lights.size(), MAX_LIGHT));
  v2f origin = renderbuffer.mapPixelToCoords(sf::Vector2i(0, 0));
  shadow_shader.setUniform("origin", origin);
  effects.lights.clear();
  effects.lights.push_back(v3f_c(player().position, 0));
  effects.lights.insert(effects.lights.begin(), static_lights.begin(), static_lights.end());

  if((settings.show_editor && status != PAUSED && gDrawShaders) || gDrawShaders)
    sfWindow.draw(temp, &shadow_shader);
  else
    sfWindow.draw(temp);

#if NUKLEAR_ENABLED
  nk_sfml_render(NK_ANTI_ALIASING_ON);
#endif
  sfWindow.resetGLStates();
  draw_osd(dt);
  sfWindow.display();
}


