#include "structs.hpp"
#include "game.hpp"


void ZPolygon::scale(sf::Vector2f factor)
{
	auto c = get_centroid();
	for (auto& line : lines)
	{
		auto bto = line.to - c, bfrom = line.from - c;
		bto.x *= factor.x;
		bto.y *= factor.y;
		bfrom.x *= factor.x;
		bfrom.y *= factor.y;
		line.to = bto + c;
		line.from = bfrom + c;
	}
}

void ZPolygon::rotate(float angle)
{
	rotation += angle * PI / 180.0f;
	auto centroid = get_centroid();
	for (auto& line : lines)
	{
		sf::Vector2f diff = line.from - centroid;
		line.from.x = centroid.x + diff.x*std::cos(angle) - diff.y*std::sin(angle);
		line.from.y = centroid.y + diff.x*std::sin(angle) + diff.y*std::cos(angle);

		diff = line.to - centroid;
		line.to.x = centroid.x + diff.x*std::cos(angle) - diff.y*std::sin(angle);
		line.to.y = centroid.y + diff.x*std::sin(angle) + diff.y*std::cos(angle);
	}
}

void ZPolygon::move(sf::Vector2f offset)
{
	for (auto& line : lines)
	{
		line.from += offset;
		line.to += offset;
	}
}

std::vector<sf::Vector2f> ZPolygon::get_points()
{
	std::set<sf::Vector2f, v2f_compare> points;
	for (auto line : lines)
	{
		points.insert(line.from);
		points.insert(line.to);
	}
	points.insert(lines.at(0).from);
	points.insert(lines.at(0).to);
	std::vector<sf::Vector2f> vec_points;
	std::copy(points.begin(), points.end(), std::back_inserter(vec_points));
	return vec_points;
}

bool ZPolygon::in_poly(sf::Vector2f point)
{
	auto points = get_points();
	int i, j, nvert = points.size();
	bool c = false;

	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((points[i].y >= point.y) != (points[j].y >= point.y)) &&
			(point.x <= (points[j].x - points[i].x) * (point.y - points[i].y) / (points[j].y - points[i].y) + points[i].x)
			)
			c = !c;
	}

	return c;
}

sf::Vector2f ZPolygon::get_centroid()
{
	const float inv = 100000.0f;
	sf::Vector2f tl(inv, inv), br(-inv, -inv);
	for (auto line : lines)
	{
		if (line.from.x + line.from.y < tl.x + tl.y) tl = line.from;
		if (line.to.x + line.to.y < tl.x + tl.y) tl = line.to;
		if (line.from.x + line.from.y > br.x + br.y) br = line.from;
		if (line.to.x + line.to.y > br.x + br.y) br = line.to;
	}
	sf::Vector2f temp = tl + (br - tl) / 2.0f;
	return temp;
}

ZPolygon ZPolygon::set_label(std::string label)
{
	texture_label = label;
	return *this;
}

void editor_action_rotate::editor_do()
{
  to_act->rotate(amount);
}

void editor_action_rotate::editor_undo()
{
  to_act->rotate(-amount);
}

void editor_action_create::editor_do()
{
  gGame.obstacles.push_back(to_add);
  added = &gGame.obstacles.back();
}

void editor_action_create::editor_undo()
{
  for(auto it = gGame.obstacles.begin(); it != gGame.obstacles.end(); it++)
  {
    if(&*it == added)
    {
      gGame.selected = nullptr;
      gGame.hovered = nullptr;
      gGame.obstacles.erase(it);
      break;
    }
  } 
}

void editor_action_point_add::editor_do()
{
  gGame.ed_points.push_back(to_add);
}

void editor_action_point_add::editor_undo()
{
  if(gGame.ed_points.size() > 0)
    gGame.ed_points.pop_back();
}

void editor_action_move::editor_do()
{
  to_act->move(to_move);
}

void editor_action_move::editor_undo()
{
  to_act->move(-to_move);
}

void editor_action_scale::editor_do()
{
  to_act->scale(to_scale);
}

void editor_action_scale::editor_undo()
{
  sf::Vector2f inv = sf::Vector2f(1.0f/to_scale.x, 1.0f/to_scale.y);
  to_act->scale(inv);
}
