#include "item.hpp"
#include <Thor/Math.hpp>
#include "utility.hpp"
#include "game.hpp"

void Gun::update()
{
  
}

bool Gun::use(Entity& user, float dt)
{
  DEB(rate, 
    {
      if(ammo > 0)
      {
        ammo--;
        gGame.create_projectile(gGame.player(), gGame.get_mouse_pos());
        return true;
      }
    }
  );

  return false;
}

bool Glock::use(Entity& user, float dt)
{
  DEB(rate,
    {
      gGame.create_projectile(gGame.player(), gGame.get_mouse_pos());
      gGame.effects.recoil_frames = 5;
      gGame.play_sound("pistol");
      v2f diff = Math::norm(gGame.player().position-gGame.get_mouse_pos());
      v2f perp = thor::perpendicularVector(diff)*(user.w+5.0f);
      user.velocity += diff*1.0f;
      gGame.create_particle(user.position+perp, user.position+perp+gGame.get_mouse_pos(), 400.0f, PARTICLE_GUN_CASING);
      gGame.effects.lights.push_back(v3f_c(user.muzzle_location));
      return true;
    }
  );
  return false;
}

bool Shotgun::use(Entity& user, float dt)
{
  
  int ox, oy;
  DEB(rate,
      {
        if(ammo > 0)
        {
          ammo--;
          gGame.effects.recoil_frames = 10;
          for(int i = 0; i < 3+rand()%spread; i++)
          {
            ox = thor::random(-1, 1);
            oy = thor::random(-1, 1);
            gGame.create_projectile(gGame.player(), gGame.get_mouse_pos()+v2f((float)ox, (float)oy));
          }
          gGame.play_sound("shotgun");
          v2f diff = Math::norm(gGame.player().position-gGame.get_mouse_pos());
          v2f perp = thor::perpendicularVector(diff)*(user.w+5.0f);
          user.velocity += diff*3.4f;
          gGame.create_particle(user.position+perp, user.position+perp+gGame.get_mouse_pos(), 400.0f, PARTICLE_SHOTGUN_CASING);
          gGame.effects.lights.push_back(v3f_c(user.muzzle_location));
        }
        else
        {
          gGame.play_sound("empty");
          gGame.effects.recoil_frames = 2;
        }
        return true;
      }
  
  );
  
  return false;
}

bool Medkit::use(Entity& user, float dt)
{
  DEB(20, 
    {
      if(user.health == user.max_health)
      {
        gGame.play_sound("error");
        gGame.bottom_message("ALREADY FULL HEALTH", sf::Color::Yellow, 30, sf::Color::Red);
        return false;
      }else{
        user.health += healing_amount;
        user.health = user.health > user.max_health ? user.max_health : user.health;
        gGame.play_sound("use");
        return true;
      }
    }
  );
  
  return false;
}

bool Pizza::use(Entity& user, float dt)
{
  return false;
}


bool Uzi::use(Entity& user, float dt)
{
  DEB(rate,
    {
      if(ammo > 0)
      {
        ammo--;
        gGame.create_projectile(gGame.player(), gGame.get_mouse_pos());
        gGame.effects.recoil_frames = 5;
        gGame.play_sound("pistol");
        v2f diff = Math::norm(gGame.player().position-gGame.get_mouse_pos());
        v2f perp = thor::perpendicularVector(diff)*(user.w+3.0f);
        user.velocity += diff*1.0f;
        gGame.create_particle(user.position+perp, user.position+perp+gGame.get_mouse_pos(), 400.0f, PARTICLE_GUN_CASING);
        gGame.effects.lights.push_back(v3f_c(user.muzzle_location));
      }else
      {
        gGame.play_sound("empty");
        gGame.effects.recoil_frames = 2;
      }
      return true;
    }
  );
  return false;
}



bool UziAkimbo::use(Entity& user, float dt)
{
  DEB(rate,
    {
      if(ammo > 0)
      {
        ammo--;
        gGame.create_projectile(gGame.player(), gGame.get_mouse_pos());
        gGame.effects.recoil_frames = 5;
        gGame.play_sound("pistol");
        v2f diff = Math::norm(gGame.player().position-gGame.get_mouse_pos());
        v2f perp = thor::perpendicularVector(diff)*(user.w+3.0f);
        user.velocity += diff*1.0f;
        gGame.create_particle(user.position+perp, user.position+perp+gGame.get_mouse_pos(), 700.0f, PARTICLE_GUN_CASING);
        gGame.effects.lights.push_back(v3f_c(user.muzzle_location));
      }else
      {
        gGame.play_sound("empty");
        gGame.effects.recoil_frames = 2;
      }
      return true;
    }
  );
  return false;
}


