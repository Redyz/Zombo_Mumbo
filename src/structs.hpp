#pragma once
#include <cmath>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Window.hpp>
#include <functional>
#include <string>
#include <vector>
#include <memory>
#include <set>
#include "enums.hpp"
class Item;
#define PI 3.1415f

struct screen_message
{
  int lifetime = 500, max_lifetime = lifetime;
  std::string text = "";
  sf::Color color = sf::Color::White, flash_color = sf::Color::White;
  int font_size = 80;
  bool done = true;
  sf::Vector2f position;
  MESSAGE_TYPE type;
  
  void set_lifetime(int time)
  {
    lifetime = time;
    max_lifetime = time;
  }
};

struct menu_item
{
  menu_item(std::string l, std::function<void(void)> a)
  : label(l)
  , action(a)
  {
    left_action = right_action = []{};
    text_update = []{ return ""; };
  }
  
  menu_item(std::function<std::string(void)> up, std::function<void(void)> a, std::function<void(void)> la, std::function<void(void)> ra, int scroll_speed_p = 1, int sound_speed_p = 30)
  : label("")
  , action(a)
  , left_action(la)
  , right_action(ra)
  , text_update(up)
  , has_left_right_options(true)
  , scroll_speed(scroll_speed_p)
  , sound_speed(sound_speed_p)
  {}
  
  std::string label;
  std::function<void(void)> action, left_action, right_action;
  std::function<std::string(void)> text_update;
  bool has_left_right_options = false;
  int scroll_speed = 1;
  int sound_speed = 30;
};

struct rules_struct
{
  float next_round_in = 0.0f;
  float max_next_round_in = 15.0f;
};

struct Line
{
  Line(float x1, float y1, float x2, float y2)
  : from(x1, y1)
  , to(x2, y2)
  {}
  
  Line(sf::Vector2f from, sf::Vector2f to)
  : from(from), to(to)
  {}
  
  sf::Vector2f center()
  {
    return sf::Vector2f((to.x-from.x)/2 + from.x, (to.y-from.y)/2 + from.y);
  }
  
  std::string str();
  
  sf::Vector2f from, to;
};

struct v2f_compare {
  bool operator() (const sf::Vector2f& lhs, const sf::Vector2f& rhs) const {
    return lhs.x+lhs.y>rhs.x+rhs.y;
  }
};

struct ZPolygon
{
  int health = 500, max_health = health;
  std::string texture_label = "cement";
  unsigned int group;
  sf::Color current_color = sf::Color::Black, old_color = current_color;
  std::string comment = "";
  std::vector<Line> lines;
  float rotation = 0;
  
  void scale(sf::Vector2f);
  void rotate(float);
  void move(sf::Vector2f);
  std::vector<sf::Vector2f> get_points();
  bool in_poly(sf::Vector2f);
  sf::Vector2f get_centroid();
  ZPolygon set_label(std::string);

} static INVALID_POLY;

struct Spawner
{
  sf::Vector2f position;
  int spawn_left = 5;
  int last_spawn = 0;
};

struct Particle
{
  int ttl = 0;
  sf::Color color;
  sf::Vector2f position;
  sf::Vector2f velocity;
  int rotation = 0;
  float size = 5.0f;
  int type = 0;
};

struct Entity
{ 
  Entity();
  void kill();

  int id;
  
  sf::Vector2f position, last_position;
  sf::Vector2f muzzle_location;
  float target_accumulator = 0.0f, next_retarget = 5.0f;
  float w = 7, h = 7;
  float speed = 20.0f;
  float birth = 0;
  unsigned int group = ENTITY_GROUP_MOB;
  unsigned int extra_group = 0;
  float rotation = 0;
  bool running = false;
  int health = 299;
  int max_health = health;
  int last_attack = 0, last_target_update = 0, last_noise = -1;
  int grenades = 1;
  int stamina = 100, max_stamina = stamina;
  std::string decal, base_sprite = "wom";
  
  int anim_frames = 0;
  int anim_state = ANIM_BOTH_ARMS, mob_state = STATE_CRUISING;
  sf::Color color;
  sf::Vector2f velocity, target;
  std::vector<std::shared_ptr<Item>> inventory;
  std::shared_ptr<Item> item;
};

struct sfml_struct
{
  sf::Font font;
};

struct Node
{
  sf::Vector2f position;
};

struct nav_cost
{
  Node target;
  float cost;
};

struct settings_struct{
  sf::Vector2f player_position;
  unsigned int max_decals = 150;
  float collision_bounce_factor = 5.0f;
  float collision_entity_bounce_factor = 1.5f;
  float friction = 0.2f;
  float texture_scale = 0.1f;
  float camera_smooth = 15.0f;
  float knockback = 1.0f;
  float max_entity_velocity = 15.0f;
  float air_friction = 0.98f;
  float energy_conservation = 0.95f;
  float attack_cooldown = 1.5f;
  float max_throw_duration = 0.6f;
  bool lock_camera_to_player = true;
  bool show_editor = false;
  int target_update = 5;
  int moan_frequency = 1500;
  int game_width = 1024, game_height = 768, back_game_width = game_width, back_game_height = game_height;
  int repair_cost = 20;
  int volume = 100;
  bool v_sync = true;
  int sf_window_style = sf::Style::Default;
  std::string map_description = "No map description";
  std::vector<sf::VideoMode> video_modes;
  sf::VideoMode current_mode;
  int selected_mode_index = 0, hovered_mode_index = 0;
} extern gSettings;

struct stats_struct
{
  static constexpr int MAX_FPS_DATA_COUNT = 100;
  std::vector<float> fps_counter_data;
  float last_fps = 0;
  int frames_since_last_reset = 0;
  sf::Clock fps_counter;
};

struct effects_struct
{
  int flash_frames = 0;
  int recoil_frames = 0;
  int max_recoil_frames = 10;
  int particles_this_frame = 0, max_particles_this_frame = 400;
  int bought_frames = 0;
  int pause_menu_flash = 0;
  std::vector<sf::Vector3f> lights;
};

struct shop_item
{
  shop_item(int p, int a, std::string t, std::function<std::shared_ptr<Item>(void)> creator);
  int price, ammo_price;
  std::string tag;
  std::function<std::shared_ptr<Item>(void)> create;
};

struct editor_action
{
  virtual void editor_do() = 0;
  virtual void editor_undo() = 0;
  virtual std::string get_label() = 0;
};

struct editor_action_rotate : editor_action
{
  editor_action_rotate(ZPolygon* to_act_p, float amount_p) 
  : to_act(to_act_p)
  , amount(amount_p) 
  {};
  std::string get_label() override { return "rotate"; };
  
  ZPolygon* to_act = nullptr;
  float amount = 0.0f;

  virtual void editor_do() override;
  virtual void editor_undo() override;
};

struct editor_action_create : editor_action
{
  editor_action_create(ZPolygon to_add_p) 
  : to_add(to_add_p)
  {};
  std::string get_label() override { return "create"; };
  
  ZPolygon to_add;
  ZPolygon* added = nullptr;
  
  virtual void editor_do() override;
  virtual void editor_undo() override;
};

/*
struct editor_action_create_node : editor_action
{
  editor_action_create_node(ZPolygon to_add_p) 
  : to_add(to_add_p)
  {};
  std::string get_label() override { return "create"; }
  
  ZPolygon to_add;
  ZPolygon* added = nullptr;
  
  virtual void editor_do() override;
  virtual void editor_undo() override;
};
*/

struct editor_action_point_add : editor_action
{
  editor_action_point_add(sf::Vector2f to_add_p) 
  : to_add(to_add_p)
  {};
  std::string get_label() override { return "point"; }
  
  sf::Vector2f to_add;
  
  virtual void editor_do() override;
  virtual void editor_undo() override;
};


struct editor_action_move : editor_action
{
  editor_action_move(ZPolygon* to_act_p, sf::Vector2f to_move_p)
  : to_act(to_act_p)
  , to_move(to_move_p)
  {};
  std::string get_label() override { return "move"; }
  
  ZPolygon* to_act;
  sf::Vector2f to_move;
  
  virtual void editor_do() override;
  virtual void editor_undo() override;
};

struct editor_action_scale : editor_action
{
  editor_action_scale(ZPolygon* to_act_p, sf::Vector2f to_scale_p)
  : to_act(to_act_p)
  , to_scale(to_scale_p)
  {};
  std::string get_label() override { return "scale"; }
  
  ZPolygon* to_act;
  sf::Vector2f to_scale;
  
  virtual void editor_do() override;
  virtual void editor_undo() override;
};
