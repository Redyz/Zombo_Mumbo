uniform sampler2D texture;
uniform vec2 resolution;
uniform vec2 cam_pos_ws;
uniform float zoom;
uniform float time;
uniform float percent_health;
uniform float percent_stamina;
uniform float percent_throw;

uniform int active_lights;

#define LIGHT_COUNT 20
uniform vec3 lights[LIGHT_COUNT];
// in -> attributes

/* LIGHT TYPES
0 = white
1 = red
2 = random
3 = blinking red
*/

vec2 ws_to_uv(vec2 toConv)
{
  toConv = toConv / (resolution*zoom);
  toConv += vec2(0.5);
  toConv.y = 1.0 - toConv.y;
  toConv -= cam_pos_ws;
  return toConv;
}

vec2 uv_to_ws(vec2 toConv)
{

  toConv.y = 1.0 - toConv.y;
  toConv -= vec2(0.5);
  toConv = toConv * (resolution*zoom);
  toConv += cam_pos_ws;
  return toConv;
}

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
  vec2 uv = gl_TexCoord[0].xy;
  vec2 uv_ws = uv_to_ws(uv);
  vec4 tex = texture2D(texture, uv);
  vec3 total = vec3(0.0, 0.0, 0.0);
  
  // lights
  for(int i = 0; i < active_lights; ++i) {
    vec2 light_ws = lights[i].xy;
    int type = int(lights[i].z);
    vec2 light_uv = ws_to_uv(light_ws);
    
    #if 0
    if(length(light_ws-uv_ws) < 50.0*zoom)
    {
      gl_FragColor = vec4(1.0);
      return;
    }
    #endif
    
    float max_dist = 70.0; 
  
    if(type == 1)
    {
      max_dist = 30.0;
    }else if(type == 2)
    {
      max_dist = 60.0 + sin(time*2.0)*30.0;
    }
    
    float len_to_light = length(uv_ws-light_ws);
    float len_to_light_norm = clamp(len_to_light, 0.0, max_dist*0.995)/max_dist;
    
    if(type == 0)
    {
      total += tex.rgb*(1.0-len_to_light_norm);
    }
    else if(type == 1)
    {
      total += vec3(tex.r*(1.0-len_to_light_norm), 0.0, 0.0);
    }
    else if(type == 2)
    {
      float r, g, b;
      r = mod(rand(vec2(i, i))/sin(time*1.0), 1.0)*(1.0-len_to_light_norm);
      g = mod(rand(vec2(i, i))/sin(time*2.0), 1.0)*(1.0-len_to_light_norm);
      b = mod(rand(vec2(i, i))/sin(time*3.0), 1.0)*(1.0-len_to_light_norm);
      total += vec3(r, g, b);
    }
    else if(type == 3)
    {
      float temp = mod(time, 1.0);
      if(temp > 0.5)
        total += vec3(tex.r*(1.0-len_to_light_norm), 0.0, 0.0);
    }
    else if(type == 4)
    {
      float r = mod(tex.r*(1.0-(len_to_light_norm*(0.9-0.1*sin(time*7.0+sin(time/2.0))))), 1.0);
      float y = mod(tex.r*(1.0-(len_to_light_norm*(0.9-0.1*sin(time*7.0+sin(time/2.0))))), 1.0);
      total += vec3(r, 0.0, 0.0);
      total += vec3(y, y, 0.0);
    }
  }

  tex = vec4(clamp(total, 0.0, 1.0), 1.0);
  
  // grain
  #if 1
  float strength = 20.0;
      
  float x = (uv.x + 4.0 ) * (uv.y + 4.0 ) * (time * 10.0);
  vec4 grain = vec4(mod((mod(x, 13.0) + 1.0) * (mod(x, 123.0) + 1.0), 0.01)-0.005) * strength;
    
  tex += grain;
  #endif

  
  //grenade
  if(uv.y > 0.990)
  {
    if(uv.x < percent_throw)
    {
      tex = vec4(0.25, 0.9, 0.25, 0.5);
    }
  }
  
  //health
  if(uv.y > 0.995)
  {
    if(uv.x < percent_health)
    {
      tex = vec4(1.0, 0.0, 0.0, 0.5);
    }
  }
  
  //stamina
  if(uv.y > 0.0 && uv.y < 0.005)
  {
    if(uv.x < percent_stamina)
    {
      tex = vec4(1.0, 1.0, 0.0, 0.5);
    }
  }
  
  gl_FragColor = tex;
}
