#!/usr/bin/env bash
current=`pwd`
binary_name=`basename $1`
binary_dir=`dirname $1`
binary="`cd "${binary_dir}";pwd`/$binary_name"
dependencies=dependencies
archive=$binary_name.tar.gz
function copy_deps()
{
  echo Copying $binary to $dependencies
  # Author : Hemanth.HM
  # Email : hemanth[dot]hm[at]gmail[dot]com
  # License : GNU GPLv3
  #
  # usage: bash $0 <path to the binary> <path to copy the dependencies>

  #Check if the paths are vaild
  [[ ! -e $binary ]] && echo "Not a vaild input $binary" && exit 1 
  [[ -d $dependencies ]] || echo "No such directory $dependencies creating..."&& mkdir -p "$dependencies"

  #Get the library dependencies
  echo "Collecting the shared library dependencies for $binary..."
  deps=$(ldd $binary | awk 'BEGIN{ORS=" "}$binary\
  ~/^\//{print $binary}$3~/^\//{print $3}'\
   | sed 's/,$/\n/')
  echo "Copying the dependencies to $dependencies"

  #Copy the deps
  for dep in $deps
  do
      echo "Copying $dep to $dependencies"
      cp "$dep" "$dependencies"
  done

  echo "Done!"
}

function package_archive()
{
  tar cvfJ $archive *
}

rm -r temp
mkdir temp
cd temp
copy_deps $binary $dependencies
cp $binary .
cp $current/../assets . -r
cp $current/../README* . 
cp $current/../scripts/run.sh .
package_archive
